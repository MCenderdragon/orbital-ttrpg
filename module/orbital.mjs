// Import document classes.
import { OrbitalActor } from './documents/actor.mjs';
import { OrbitalItem } from './documents/item.mjs';
// Import sheet classes.
import { OrbitalActorSheet } from './sheets/actor-sheet.mjs';
import { OrbitalItemSheet } from './sheets/item-sheet.mjs';
// Import helper/utility classes and constants.
import { preloadHandlebarsTemplates } from './helpers/templates.mjs';
import { BOILERPLATE, ORBITAL } from './helpers/config.mjs';

import {getChanceLvl, getChanceWurfel, booleanFix} from './helpers/combat.mjs'

import {onChatDamageRoll, onChatDefendRoll, onChatApplyDamageRoll} from './helpers/actor-attack-dialog.mjs'

/* -------------------------------------------- */
/*  Init Hook                                   */
/* -------------------------------------------- */

Hooks.once('init', function () {

	console.log(`
	------------------------------------------------------

	 ██████╗ ██████╗ ██████╗ ██╗████████╗ █████╗ ██╗     
	██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝██╔══██╗██║     
	██║   ██║██████╔╝██████╔╝██║   ██║   ███████║██║     
	██║   ██║██╔══██╗██╔══██╗██║   ██║   ██╔══██║██║     
	╚██████╔╝██║  ██║██████╔╝██║   ██║   ██║  ██║███████╗
	 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝
	======================================================                                                     

`);
	// Add utility classes to the global game object so that they're more easily
	// accessible in global contexts.
	game.orbital = {
		OrbitalActor,
		OrbitalItem,
		rollItemMacro,
	};

	// Add custom constants for configuration.
	CONFIG.BOILERPLATE = BOILERPLATE;
	CONFIG.ORBITAL = ORBITAL;

	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
	CONFIG.Combat.initiative = {
		formula: '1d4 + @resources.initiative.value',
		decimals: 2,
	};

	// Define custom Document classes
	CONFIG.Actor.documentClass = OrbitalActor;
	CONFIG.Item.documentClass = OrbitalItem;

	// Active Effects are never copied to the Actor,
	// but will still apply to the Actor from within the Item
	// if the transfer property on the Active Effect is true.
	CONFIG.ActiveEffect.legacyTransferral = false;

	CONFIG.statusEffects = ORBITAL.effects;
	CONFIG.specialStatusEffects = ORBITAL.specialEffects;

	// Register sheet application classes
	Actors.unregisterSheet('core', ActorSheet);
	Actors.registerSheet('orbital', OrbitalActorSheet, {
		makeDefault: true,
		label: 'ORBITAL.SheetLabels.Actor',
	});
	Items.unregisterSheet('core', ItemSheet);
	Items.registerSheet('orbital', OrbitalItemSheet, {
		makeDefault: true,
		label: 'ORBITAL.SheetLabels.Item',
	});

	// Preload Handlebars templates.
	return preloadHandlebarsTemplates();
});

/* -------------------------------------------- */
/*  Handlebars Helpers                          */
/* -------------------------------------------- */

// If you need to add Handlebars helpers, here is a useful example:
Handlebars.registerHelper('toLowerCase', function (str) {
	return str.toLowerCase();
});

Handlebars.registerHelper('equals', function (a, b) 
{
	return a === b;
});

Handlebars.registerHelper('booleanFix', booleanFix);

function calcChance(min, max) 
{
	const lvlA = 21 - min;
	const lvlB = max - 21;
	let chance = 0;
	if( lvlA == lvlB)
	{
		chance = getChanceLvl(lvlA);
	}
	else
	{
		chance = getChanceWurfel(min, max);
	}
	return Math.floor(chance * 100) + "%";
}

Handlebars.registerHelper('calcChance', calcChance);

Handlebars.registerHelper('getFrom', function (map, key) 
{
	try
	{
		return map[key];
	}
	catch(err)
	{
		console.error(err, map, key);
		return "!!!ERROR!!!";
	}
});

Handlebars.registerHelper('showDice', function (wurfel) 
{
	if(wurfel)
	{
		const tooltiptext = `21 &#xB1; ${wurfel.tooltip}`;
		const html = `<span class="wurfel" data-tooltip='${tooltiptext}'> &#x1F3B2; <span class="grid-span-1">${wurfel.min} - ${wurfel.max} (${calcChance(wurfel.min, wurfel.max)})</span></span>`;
		
		return new Handlebars.SafeString(html);
	}
	else
	{
		return "<NO WURFEL OBJECT PROVIDED>"
	}
});

/* -------------------------------------------- */
/*  Ready Hook                                  */
/* -------------------------------------------- */

Hooks.once('ready', function () {
	// Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
	Hooks.on('hotbarDrop', (bar, data, slot) => createItemMacro(data, slot));
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createItemMacro(data, slot) {
	// First, determine if this is a valid owned item.
	if (data.type !== 'Item') return;
	if (!data.uuid.includes('Actor.') && !data.uuid.includes('Token.')) {
		return ui.notifications.warn(
			'You can only create macro buttons for owned Items'
		);
	}
	// If it is, retrieve it based on the uuid.
	const item = await Item.fromDropData(data);

	// Create the macro command using the uuid.
	const command = `game.orbital.rollItemMacro("${data.uuid}");`;
	let macro = game.macros.find(
		(m) => m.name === item.name && m.command === command
	);
	if (!macro) {
		macro = await Macro.create({
			name: item.name,
			type: 'script',
			img: item.img,
			command: command,
			flags: { 'orbital.itemMacro': true },
		});
	}
	game.user.assignHotbarMacro(macro, slot);
	return false;
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemUuid
 */
function rollItemMacro(itemUuid) {
	// Reconstruct the drop data so that we can load the item.
	const dropData = {
		type: 'Item',
		uuid: itemUuid,
	};
	// Load the item from the uuid.
	Item.fromDropData(dropData).then((item) => {
		// Determine if the item loaded and if it's an owned item.
		if (!item || !item.parent) {
			const itemName = item?.name ?? itemUuid;
			return ui.notifications.warn(
				`Could not find item ${itemName}. You may need to delete and recreate this macro.`
			);
		}

		// Trigger the item roll
		item.roll();
	});
}

/**
 * @param {ChatMessage} message
 * @param {jQuery} html
 * @param {any} messageData
 */
Hooks.on("renderChatMessage", (message, html, messageData) => {
	
	html.on('click', '.chat-defend-roll', onChatDefendRoll);
	html.on('click', '.chat-damage-roll', onChatDamageRoll);
	html.on('click', '.chat-apply-damage-roll', onChatApplyDamageRoll);
});