/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function () {
  return loadTemplates([
    // Actor partials.
    'systems/orbital/templates/actor/parts/actor-features.hbs',
    'systems/orbital/templates/actor/parts/actor-items.hbs',
    'systems/orbital/templates/actor/parts/actor-spells.hbs',
    'systems/orbital/templates/actor/parts/actor-effects.hbs',
    'systems/orbital/templates/actor/parts/actor-attributes.hbs',
    'systems/orbital/templates/actor/parts/actor-skills.hbs',
    'systems/orbital/templates/actor/parts/actor-description.hbs',
    'systems/orbital/templates/actor/parts/actor-equipment.hbs',
    // Item partials
    'systems/orbital/templates/item/parts/item-effects.hbs',
  ]);
};
