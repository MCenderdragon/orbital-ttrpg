import {
	calcAttack,
	attackTargetToken, 
	calculateItemAttackChancesForDistance, 
	calcArmor, 
	getDamageLogic
} from '../helpers/combat.mjs'


/**
 * 
 * @param {Array<Token>} targets 
 * @param {float} distance 
 */
function findToken(targets, distance)
{
	const target = targets.find(t => t.distance == distance);
	if(target)
		return target;
	else
	{
		return targets.find(t => t.distance <= distance);
	}
}

/**
 * 
 * @param {boolean} success 
 * @param {boolean} krit 
 * @returns {Number}
 */
function getIntFromRollResult(success, krit)
{
	return (success ? 0b1 : 0) | (krit ? 0b10 : 0)
}

/**
 * 
 * @param {Number} rollResult 
 * @returns {Object} "success":success, "krit":krit
 */
function getRollResultfromInt(rollResult)
{
	const success = (rollResult & 0b1) == 0b1;
	const krit = (rollResult & 0b10) == 0b10;
	return {"success":success, "krit":krit};
}


/**
 * 
 * @param {ActorSheet} actorSheet 
 */
export function open(actorSheet)
{
	const user = game.user.targets.user;
	var targetHtml = "<p>No Target defined choose range</p>";
	const targets = [];
	const tokenDoc = actorSheet.token;
	//console.log(actorSheet)
	if(tokenDoc)
	{
		const localScene = tokenDoc.parent;
		//console.log(game.user.targets)
		let filtered = game.user.targets.filter(token => token.scene == localScene);
		//console.log("_onActorAttack", filtered)
		if(filtered.size > 0)
		{
			targetHtml = "<p>Targets</p>";
			
			filtered.forEach(t => targets.push({"token": t, "distance": (canvas.grid.measurePath([tokenDoc, t]).distance)})); //grid is defined as square with 3m
			targets.sort((a, b) => a.distance - b.distance);

			targets.forEach(t => targetHtml += `
			<li class='item flexrow'>
				<div class='item-image'>
						<img
						src='${t.token.document.texture.src}'
						title='${t.token.document.name}'
						width='24'
						height='24'
						/>
				</div>
				<span>${t.token.document.name}</span>
				<span>Distance: ${t.distance}</span>
			</li>`)



			//console.log(this, targets);
			//#attackTargetToken(actorSheet.actor, tokenDoc, targets[0]);
			//return;
		}
	}
	

	{
		const dialog = new foundry.applications.api.DialogV2(
		{
			window: { title: "Choose an option" },
			content: `
				${targetHtml}
				<label>Range <input type="number" name="range" value="3"></label>
			`,
			buttons: [
				{
					action: "confirm",
					label: game.i18n.localize("orbital.actor.attack.attack"),
					default: true,
					callback: (event, button, dialog) => button.form.elements.range.value
				},
				{
					action: "damage",
					label: game.i18n.localize("orbital.actor.attack.damage_roll"),
				}
			],
			submit: async result => {

				const attack = calcAttack(actorSheet.actor)

				if(result === "damage")
				{
					const roll = new Roll("1D4", actorSheet.actor.getRollData());
					await roll.roll();
					
					const formula = `🩸 ${attack.ATK}${attack.ATK > attack.BET ? " + 1D4" : ""} ⚡ ${attack.BET}${attack.ATK < attack.BET ? " + 1D4" : ""}`;
					const dieA = roll.terms[0].results[0].result;
					const total = `🩸 ${attack.ATK +((attack.ATK > attack.BET) ? dieA : 0)} ⚡ ${attack.BET +((attack.ATK < attack.BET) ? dieA : 0)}`;

					ChatMessage.create({
						content: `
						<div class="dice-roll">
							<div class="dice-result">
								<div class="dice-formula">${formula}</div>
								<div class="dice-tooltip expanded" style="display: block;">
									<section class="tooltip-part">
										<div class="dice">
											<ol class="dice-rolls">
												<li class="roll die d4">${dieA}</li>
											</ol>
										</div>
									</section>
								</div>
								<h4 class="dice-total"> ${total}</h4>
							</div>
							<div >${game.i18n.localize("orbital.actor.attack.damage_roll")}</div>
						</div>
						`,
						speaker: ChatMessage.getSpeaker({ actor: actorSheet.actor }),
						sound: CONFIG.sounds.dice
					});
				}
				else
				{
					const attackChance = calculateItemAttackChancesForDistance(actorSheet.actor.getItem("mainhand"), result);
					const itemName = attackChance.item?.name ?? "-";
					const deltaChance = attackChance.deltaChance;
					const attackType = attackChance.combatType;
					const target = findToken(targets, result)

					console.log("main", attackChance, target);

					const targetName = target?.token.document.name ?? "?";  // var?. returns undefined and crashes not
					const targetUUID = target?.token.document.uuid ?? "-";  // var ?? default , returnes default when var i null or undefained - so it works with false correctly


					if(deltaChance!=0)
					{
						attack.wurfel.dice.addModifier("attack_range", deltaChance, true);
						attack.wurfel = attack.wurfel.dice.asJson();
					}
					console.log("actorsheet", actorSheet)
					
					actorSheet._do2D20Roll(attack.wurfel, `${itemName} ⚔️ ${targetName}`, `attack.${attackType}`, 
						(dieA, dieB, success, krit, wurfel) => {
							return `<button class="dice-result chat-defend-roll" 
										data-attacker="${actorSheet.token.uuid}" 
										data-target="${targetUUID}" 
										data-type=${attackType}
										data-attack_result=${getIntFromRollResult(success, krit)}
									>(Target) Defend</button>`
					});
				}
			}
		});
		dialog.render({ force: true });
	}
}

/**
 * 
 * @param {TokenDocument} targetDoc 
 * @param {TokenDocument} attackerDoc 
 */
function makeNotification(targetDoc, attackerDoc)
{
	if(targetDoc && !targetDoc.isOwner)
		ui.notifications.warn(`You must be the owner of the Token ${targetDoc.name}`);
	else if(!targetDoc)
		ui.notifications.error(`Target Token not found`);
	else if(!attackerDoc)
		ui.notifications.error(`Attacker Token not found`);
	else
		ui.notifications.error(`unknown error`);
}

export async function onChatDefendRoll(event)
{
	event.preventDefault();
	event.target.disabled = true;

	const header = event.currentTarget;
	// Get the type of item to create.
	const promieseUuidTarget = fromUuid(header.dataset.target);
	const promieseUuidAttacker = fromUuid(header.dataset.attacker);
	const attackType = header.dataset.type;
	const attackResult = header.dataset.attack_result;

	var armorType = null;
	switch(attackType) {
		case "close":
		case "ranged":
		case "magic":
			armorType = attackType;
			break;
		case "longdistance":
			armorType = "ranged";
			break;
		default:
			return;
	}

	const targetDoc = await promieseUuidTarget;
	const attackerDoc = await promieseUuidAttacker;

	if(targetDoc && targetDoc.isOwner && attackerDoc)
	{
		//do Defend Roll
		if(targetDoc.actor)
		{
			const armor = calcArmor(targetDoc.actor);
			console.log("armor", armor, armor.chances[armorType]);
			const wurfel = armor.chances[armorType].wurfel;
			console.log("token target", targetDoc, attackerDoc)
			targetDoc.actor.sheet._do2D20Roll(wurfel, `🛡️ ⚔️ ${attackerDoc?.name}`, `defend.${armorType}`, 
				(dieA, dieB, success, krit, wurfel) => {

				const logic = getDamageLogic(attackerDoc, targetDoc, attackType, getRollResultfromInt(attackResult), {"success":success, "krit":krit});
				let html = "";

				console.log("damage Logic", logic)

				logic.forEach(e =>{ //{"attacker":attackerDoc, "target":targetDoc, "damage_dice": dmgDice, "apply_armor": applyArmor, "apply_shield":applyShield},
					html += `<button class="dice-result chat-damage-roll" 
							data-attacker="${e.attacker.uuid}" 
							data-target="${e.target.uuid}" 
							data-type=${e.attack_type}
							data-damage_dice=${e.damage_dice}
							data-apply_armor=${e.apply_armor}
							data-apply_shield=${e.apply_shield}
							>Roll Damage</button>`
				});

				return html;
			});
		}
	}
	else
	{
		makeNotification(targetDoc, attackerDoc);
	}
}



export async function onChatDamageRoll(event)
{
	event.preventDefault();
	event.target.disabled = true;

	const header = event.currentTarget;
	// Get the type of item to create.
	const promieseUuidTarget = fromUuid(header.dataset.target);
	const promieseUuidAttacker = fromUuid(header.dataset.attacker);
	const attackType = header.dataset.type;

	const damage_dice = header.dataset.damage_dice;
	const apply_armor = header.dataset.apply_armor;
	const apply_shield = header.dataset.apply_shield;

	const targetDoc = await promieseUuidTarget;
	const attackerDoc = await promieseUuidAttacker;

	if(targetDoc && targetDoc.isOwner && attackerDoc)
	{
		const attack = calcAttack(attackerDoc.actor)
		const bonusDamage = !!attackerDoc.actor.getItem("mainhand");

		const roll = new Roll(damage_dice, attackerDoc.actor.getRollData());
		await roll.roll();
		
		const formula = `🩸 ${attack.ATK}${bonusDamage && attack.ATK > attack.BET ? " + "+damage_dice : ""} ⚡ ${attack.BET}${bonusDamage && attack.ATK < attack.BET ? " + "+damage_dice : ""}`;
		const dieA = roll.terms[0].results[0].result;
		const ATK = attack.ATK +((bonusDamage &&  attack.ATK > attack.BET) ? dieA : 0)
		const BET = attack.BET +((bonusDamage &&  attack.ATK < attack.BET) ? dieA : 0)
		const total = `🩸 ${ATK} ⚡ ${BET}`;

		ChatMessage.create({
			content: `
			<div class="dice-roll">
				<div class="dice-result">
					<div class="dice-formula">${formula}</div>
					<div class="dice-tooltip expanded" style="display: block;">
						<section class="tooltip-part">
							<div class="dice">
								<ol class="dice-rolls">
									<li class="roll die d4">${dieA}</li>
								</ol>
							</div>
						</section>
					</div>
					<h4 class="dice-total"> ${total}</h4>
				</div>
				<div >${game.i18n.localize("orbital.actor.attack.damage_roll")}</div>
				<button class="dice-result chat-apply-damage-roll" 
							data-attacker="${header.dataset.attacker}" 
							data-target="${header.dataset.target}" 
							data-type=${attackType}
							data-damage_atk=${ATK}
							data-damage_bet=${BET}
							data-apply_armor=${apply_armor}
							data-apply_shield=${apply_shield}
							>apply Damage</button>
			</div>
			`,
			speaker: ChatMessage.getSpeaker({ actor: attackerDoc.actor }),
			sound: CONFIG.sounds.dice
		});
	}
	else
	{
		makeNotification(targetDoc, attackerDoc);
	}
}

export async function onChatApplyDamageRoll(event)
{
	event.preventDefault();
	event.target.disabled = true;

	const header = event.currentTarget;
	// Get the type of item to create.
	const promieseUuidTarget = fromUuid(header.dataset.target);
	const promieseUuidAttacker = fromUuid(header.dataset.attacker);
	const attackType = header.dataset.type;

	const ATK = header.dataset.damage_atk;
	const BET = header.dataset.damage_bet;

	const apply_armor = header.dataset.apply_armor;
	const apply_shield = header.dataset.apply_shield;

	const targetDoc = await promieseUuidTarget;
	const attackerDoc = await promieseUuidAttacker;

	if(targetDoc && targetDoc.isOwner && attackerDoc)
	{
		const armor = calcArmor(targetDoc.actor, apply_armor, apply_shield); //shield_phys shield_mind
		let fATK=ATK;
		let fBET=BET;
		let damageString = `🩸 ${ATK} ⚡ ${BET}`;
		if(attackType == "magic")
		{
			fATK -= armor.shield_mind;
			fBET -= armor.shield_mind;
			damageString += ` - 🧠 ${armor.shield_mind}`
		}
		else
		{
			fATK -= armor.shield_phys;
			fBET -= armor.shield_phys;
			damageString += ` - 🛡️ ${armor.shield_phys}`
		}
		if(fATK<0)
			fATK=0;
		if(fBET<1)
			fBET=1;

		damageString += ` = 🩸 ${fATK} ⚡ ${fBET}`;

		console.log("armor apply damage", armor);
		const info = `🎯 ${targetDoc.name}<br>
		⚜️: ${apply_armor} 🛡️:${apply_shield}<br>
		Type:${attackType}<br>
		⚔️:${attackerDoc.name}`

		const beforeDamage = `♥️ ${targetDoc.actor.system.resources.health.value} 🔄 ${targetDoc.actor.system.resources.initiative.value}`;

		targetDoc.actor.system.resources.health.value -= fATK;
		targetDoc.actor.system.resources.initiative.value -= fBET;

		await targetDoc.actor.update({'system.resources' : targetDoc.actor.system.resources});

		const afterDamage = `♥️ ${targetDoc.actor.system.resources.health.value} 🔄 ${targetDoc.actor.system.resources.initiative.value}`;

		ChatMessage.create({
			content: `
			<div class="dice-roll">
				<div class="dice-result">
					<div class="dice-formula">${info}</div>
					<div class="dice-formula">${beforeDamage}</div>
					<div class="dice-formula">${damageString}</div>
					<h4 class="dice-total"> ${afterDamage}</h4>
				</div>
			</div>
			`,
			speaker: ChatMessage.getSpeaker({ actor: targetDoc.actor })
		});

	}
	else
	{
		makeNotification(targetDoc, attackerDoc);
	}

}
