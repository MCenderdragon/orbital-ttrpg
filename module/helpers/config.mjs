export const BOILERPLATE = {};
export const ORBITAL = {};

/**
 * The set of Ability Scores used within the system.
 * @type {Object}
 */
BOILERPLATE.abilities = {
  str: 'BOILERPLATE.Ability.Str.long',
  dex: 'BOILERPLATE.Ability.Dex.long',
  con: 'BOILERPLATE.Ability.Con.long',
  int: 'BOILERPLATE.Ability.Int.long',
  wis: 'BOILERPLATE.Ability.Wis.long',
  cha: 'BOILERPLATE.Ability.Cha.long',
};

BOILERPLATE.abilityAbbreviations = {
  str: 'BOILERPLATE.Ability.Str.abbr',
  dex: 'BOILERPLATE.Ability.Dex.abbr',
  con: 'BOILERPLATE.Ability.Con.abbr',
  int: 'BOILERPLATE.Ability.Int.abbr',
  wis: 'BOILERPLATE.Ability.Wis.abbr',
  cha: 'BOILERPLATE.Ability.Cha.abbr',
};

ORBITAL.attribute_list = {
"STR": "orbital.attribute.STR.long",
"RES": "orbital.attribute.RES.long",
"REF": "orbital.attribute.REF.long",
"GES": "orbital.attribute.GES.long",
"AUD": "orbital.attribute.AUD.long",
"INT": "orbital.attribute.INT.long",
"WIL": "orbital.attribute.WIL.long",
"PEZ": "orbital.attribute.PEZ.long",
"AUR": "orbital.attribute.AUR.long",
"CHA": "orbital.attribute.CHA.long"
};

ORBITAL.equipment_slots = {
  "head": {"max": 1, "isArmor": true, "weight": 2},
  "body": {"max": 1, "isArmor": true, "weight": 10},
  "boots": {"max": 1, "isArmor": true},
  "gloves": {"max": 1, "isArmor": true},
  "NONE": {"max": 0, "isArmor": true},
  "misc": {"max": 3, "isArmor": true, "weight": 1},
  "mainhand": {"max": 1, "isArmor": false, "weight": 5},
  "offhand": {"max": 1, "isArmor": false, "weight": 5},
  "support": {"max": 5, "isArmor": false, "weight": 1}
};

ORBITAL.nahkampf_attribute = {
  "STR": {"two_handed": true, "one_handed": false, "schilde": false, "can_block": true},
  "GES": {"two_handed": false, "one_handed": true, "schilde": true, "can_block": false},
  "REF": {"two_handed": true, "one_handed": false, "schilde": false, "can_block": false},
  "AUD": {"two_handed": true, "one_handed": false, "schilde": false, "can_block": false},
  "INT": {"two_handed": false, "one_handed": true, "schilde": true, "can_block": false},
  "PEZ": {"two_handed": false, "one_handed": true, "schilde": false, "can_block": false}, //blocking with Schwertmeister
  "AUR": {"two_handed": true, "one_handed": true, "schilde": true, "can_block": false}    //blockign with Paladin & two handed
};

ORBITAL.fernkampf_attribute = {
  "STR": {"two_handed": true, "one_handed": false, "schilde": true, "can_block": false},
  "GES": {"two_handed": false, "one_handed": true, "schilde": true, "can_block": false},
  "REF": {"two_handed": true, "one_handed": false, "schilde": true, "can_block": false},
  "AUD": {"two_handed": true, "one_handed": false, "schilde": true, "can_block": false},
  "INT": {"two_handed": true, "one_handed": false, "schilde": false, "can_block": false},
  "PEZ": {"two_handed": true, "one_handed": false, "schilde": false, "can_block": false},
  "AUR": {"two_handed": true, "one_handed": true, "schilde": true, "can_block": false}
};

const STR = "STR";
const RES = "RES";
const REF = "REF";
const GES = "GES";
const AUD = "AUD";
const INT = "INT";
const WIL = "WIL";
const PEZ = "PEZ";
const AUR = "AUR";
const CHA = "CHA";

ORBITAL.armor_phys = [STR, RES, REF, GES, AUD];
ORBITAL.armor_mind = [INT, WIL, PEZ, AUR, CHA];

//Base
ORBITAL.skills = {
  "-": { "-": "" }
};
//merge additional
ORBITAL.skills = Object.assign(ORBITAL.skills, {
  'Raumpilot': { 'Keine': 'GES', 'Flugzeug': 'GES', 'Kampfpilot': 'REF', 'Bomber': 'PEZ', 'Covert-Ops': 'PEZ', 'Großkampfschiff': 'INT' },
  'Fahrer': { 'Keine': 'GES', 'Mechpilot': 'REF', 'Panzer': 'GES', 'Transport': 'PEZ' },
  'Steuermann': { 'Keine': 'GES', 'Schiffe': 'GES', 'Luftkissenboot': 'GES', 'U-Boot': 'INT' },
  'Schütze': { 'Keine': 'GES', 'Sniper': 'PEZ', 'Schwer': 'AUD', 'Board': 'REF', 'Ballistisch': 'STR', 'Kommando': 'REF', 'Manipulator': 'AUR' },
  'Nahkämpfer': { 'Keine': 'GES', 'Schwertmeister': 'PEZ', 'Mechanisiert': 'AUD', 'Attentäter': 'REF', 'Paladin': 'AUR', 'Kriegswaffen': 'STR', 'Waffenlos': 'INT' },
  'Verteidiger': { 'Keine': 'RES' },
  'Athlet': { 'Keine': 'GES', 'Pakour': 'REF', 'Klettern': 'AUD', 'Werfen': 'GES', 'Ausweichen': 'REF', 'Schleichen': 'PEZ' },
  'Kommandant': { 'Keine': 'CHA', 'Befehlshaber': 'CHA', 'Einsatzleiter': 'CHA' },
  'Händler': { 'Keine': 'CHA', 'Schmuggler': 'CHA', 'Wirtschaft': 'INT', 'Gutachter': 'PEZ', 'Waffen': 'WIIL', 'Ressourcen': 'PEZ', 'High-Tec': 'INT', 'Nahrung': 'PEZ', 'Chassis': 'INT', 'Medizin': 'INT' },
  'Diplomat': { 'Keine': 'CHA', 'Überzeugen': 'CHA', 'Bestechen': 'WIL', 'Etikette': 'INT', 'Beobachter': 'PEZ', 'Unterhändler': 'AUR' },
  'Gauner': { 'Keine': 'WIL', 'Erpressen': 'WIL', 'Verhören': 'AUD', 'Bedrohen': 'STR', 'Taschendieb': 'REF', 'Verführen': 'CHA' },
  'Informant': { 'Keine': 'INT', 'Broker': 'CHA', 'Spion': 'PEZ', 'Dedektiv': 'AUD' },
  'Mediziner': { 'Keine': 'PEZ', 'Feldsanitäter': 'PEZ', 'Chirurg': 'INT', 'Forscher': 'PEZ', 'Tierarzt': 'GES' },
  'Mechaniker': { 'Keine': 'GES', 'Bergung': 'PEZ', 'Reperatur': 'GES', 'Schmied': 'STR', 'Elektrik': 'INT', 'Saboteur': 'GES' },
  'Ingenieur': { 'Keine': 'INT', 'Antrieb': 'PEZ', 'Schusswaffen': 'GES', 'Ausrüstung': 'AUD', 'Robotik': 'INT' },
  'Quantentechniker': { 'Keine': 'INT', 'Sprungantrieb': 'INT', 'Tarnfeld': 'PEZ', 'Energietechnik': 'GES', 'Rüstung': 'AUD', 'Sensoren': 'INT' },
  'Auratechniker': { 'Keine': 'AUR', 'Schilde': 'AUR', 'Ausrüstung': 'AUR', 'Tarnfeld': 'PEZ', 'Sensoren': 'AUR', 'Sprungantrieb': 'AUR' },
  'Geologe': { 'Keine': 'AUD', 'Geologie': 'INT', 'Raffinerie': 'PEZ', 'Panzerung': 'AUD' },
  'Chemiker': { 'Keine': 'INT', 'Medikamente': 'PEZ', 'Treibstoff': 'RES', 'Forensik': 'PEZ' },
  'Biologe': { 'Keine': 'INT', 'Biomechanik': 'GES', 'Genetik': 'PEZ', 'Botanik': 'AUD', 'Tierhaltung': 'CHA' },
  'Informatiker': { 'Keine': 'INT', 'IT-Techniker': 'INT', 'Hacker': 'PEZ', 'Datenanalyse': 'AUD', 'Kryptik': 'AUR' },
  'Astronom': { 'Keine': 'PEZ', 'Astrometrie': 'INT', 'Objektanalyse': 'PEZ', 'Observierung': 'AUD' },
  'Navigator': { 'Keine': 'INT', 'Kartographie': 'INT', 'Nautik': 'PEZ' },
  'Kochen': { 'Keine': 'GES', 'Heilkunde': 'PEZ', 'Brauer': 'RES' },
  'Angeln': { 'Keine': 'AUD' },
  'Zechen': { 'Keine': 'RES' },
  'Archäologe': { 'Keine': 'INT', 'Kryptograph': 'PEZ', 'Historiker': 'INT' },
  'Psychologe': { 'Keine': 'INT', 'Therapeut': 'CHA', 'Profiler': 'PEZ' },
  'Taucherlunge': { 'Keine': 'AUD' },
  'Pyrotechniker': { 'Keine': 'GES', 'Kampfmittel': 'PEZ', 'Bergbau': 'GES' },
  'Jäger': { 'Keine': 'PEZ', 'Grabräuber': 'AUD', 'Großwild': 'GES', 'Kleinwild': 'REF', 'Schnitzel': 'AUR' },
  'Occultist': { 'Keine': 'AUR' },
  'Jurist': { 'Keine': 'INT' }
});

ORBITAL.races = {
  'Menschen': {
    'maxAge': '150',
    'adultAge': '10',
    'sex': ['Männlich', 'Weiblich'],
    'genom': 'Humanoid/Primat',
    'height': { 'min': '1,50 m', 'max': '2,10 m' },
    'character': ['Geweissenhaft', 'Misstrauisch'],
    'raceskill': 'Ausgewogen',
    'canFly': 0,
    'playerRace': 1,
    'companionRace': 0,
    'desc': 'Der Klassiker'
  }
};
ORBITAL.races = Object.assign(ORBITAL.races,
  {
    'Menschen': {
      'maxAge': '150', 'adultAge': '10', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Primat', 'height': { 'min': '1,50 m', 'max': '2,10 m' },
      'character': ['Geweissenhaft', 'Misstrauisch'], 'raceskill': 'Ausgewogen', 'canFly': 0, 'playerRace': 1,
      'companionRace': 0, 'desc': 'Der Klassiker'
    },
    'Witcheriana': {
      'maxAge': '300', 'adultAge': '14', 'sex': ['Metamorph', 'Weiblich'], 'genom': 'Humanoid/Kobold', 'height': { 'min': '1,30 m', 'max': '1,65 m' },
      'character': ['Geheimnissvoll', 'Überheblich'], 'raceskill': 'Auratechnik', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Sagengestallten der Erde (Hexen, Zauberer), hoher Entwicklungsstand'
    },
    'Kamelianer': {
      'maxAge': '80', 'adultAge': '12', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Echse/Beuteltier', 'height': { 'min': '1,60 m', 'max': '1,70 m' },
      'character': ['Empatisch', 'Introvertiert'], 'raceskill': 'Tarnung', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Wenig angesehen bei anderen Rassen'
    },
    'Aranganer': {
      'maxAge': '60', 'adultAge': '8', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Nagetier/Humanoid', 'height': { 'min': '1,50 m', 'max': '2,20 m' },
      'character': ['Gesellig', 'Aufdringlich'], 'raceskill': 'schnelle  Reflexe', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Riesen Hasen auf zwei Beinen'
    },
    'Nion': {
      'maxAge': '500', 'adultAge': '40', 'sex': ['Hermaphrodit', 'Neutrum'], 'genom': 'Echse/Insekt', 'height': { 'min': '1,70 m', 'max': '2,30 m' },
      'character': ['Spirituell', 'Überheblich'], 'raceskill': 'Wissenschaft', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Insekten mit Nanobots -> Nio Modifikator ist Pflicht (Kultur), "Mantis"'
    },
    'Baran': {
      'maxAge': '120', 'adultAge': '16', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Schwein/Kobold', 'height': { 'min': '1,20 m', 'max': '1,60 m' },
      'character': ['Ehrlich', 'Streitsüchtig'], 'raceskill': 'Robust', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"Ork" Schweinenase'
    },
    'Lyxaner': {
      'maxAge': '90', 'adultAge': '12', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Katze/Humanoid', 'height': { 'min': '1,40 m', 'max': '1,80 m' },
      'character': ['Neugirig', 'Skrupellos'], 'raceskill': 'Kommandieren', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Katzenmensch (Wildkatze)'
    },
    'Glorb': {
      'maxAge': '480', 'adultAge': '4', 'sex': ['Metamorph', ''], 'genom': 'Schleim', 'height': { 'min': ',50 m', 'max': '3,0 m' },
      'character': ['Fleisig', 'Respektlos'], 'raceskill': 'Formwandeln', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Teilung, kein Verständniss für Privatsspähre'
    },
    'Pierader': {
      'maxAge': '800', 'adultAge': '20', 'sex': ['Hermaphrodit', ''], 'genom': 'Humanoid/Vogel', 'height': { 'min': '1,70 m', 'max': '2,30 m' },
      'character': ['Rechtschafend', 'Eitel'], 'raceskill': 'Auffassungsgabe', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': 'Vogelmensch - evtl Relegiös'
    },
    'Diablos [Martyria]': {
      'maxAge': '950', 'adultAge': '12', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Dämon', 'height': { 'min': '1,80 m', 'max': '2,60 m' },
      'character': ['Stark', 'Tempramentvoll'], 'raceskill': 'Kämpfer', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'meist Hufe, magisch begabt'
    },
    'Succubus [Martyria]': {
      'maxAge': '1200', 'adultAge': '40', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Dämon', 'height': { 'min': '1,40 m', 'max': '2,0 m' },
      'character': ['Attraktiv', 'Unzüchtig'], 'raceskill': 'Verführung', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': 'kann Speziesmerkmale(hörner, flügel) verbergen, nur alle 40 Jahre Zeugungsfähig'
    },
    'Elementar [Martyria]': {
      'maxAge': 'keine', 'adultAge': '12', 'sex': ['Metamorph', ''], 'genom': 'Humanoid/Dämon', 'height': { 'min': '1,80 m', 'max': '3,0 m' },
      'character': ['Beschützer', 'Destruktiv'], 'raceskill': 'Elementarmystiken', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': ''
    },
    'Golga': {
      'maxAge': '120', 'adultAge': '10', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Schleim', 'height': { 'min': '1,60 m', 'max': '1,80 m' },
      'character': ['Hilfsbereit', 'Närrisch'], 'raceskill': 'Heilen', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'dumm, leichtgläubig, formwandler, masochistisch'
    },
    'Fenrianer': {
      'maxAge': '60', 'adultAge': '6', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Dämon/Wolf', 'height': { 'min': '1,60 m', 'max': '2,30 m' },
      'character': ['Zielstrebig', 'Agressiv'], 'raceskill': 'Attentäter', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"Wehrwolf", gute Soldaten, Schamanisctisch'
    },
    'Taurion': {
      'maxAge': '80', 'adultAge': '8', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Huftier', 'height': { 'min': '1,80 m', 'max': '2,60 m' },
      'character': ['Genügsam', 'Dickköpfig'], 'raceskill': 'Ausdauernd', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"Minotauros", gute Handwerker'
    },
    'Equvarum': {
      'maxAge': '40', 'adultAge': '6', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Huftier/Mutant', 'height': { 'min': '1,60 m', 'max': '2,10 m' },
      'character': ['Arbeitsam', 'Ungeschickt'], 'raceskill': 'Transport', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Pferd mit Armen am Rücken um sich zu Beladen'
    },
    'Catrill': {
      'maxAge': '40', 'adultAge': '6', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Katze/Kobold', 'height': { 'min': '1,20 m', 'max': '1,40 m' },
      'character': ['Kreativ', 'Hecktisch'], 'raceskill': 'Handwerkskunst', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Katzenhaft, gern in Gruppen'
    },
    'Obori': {
      'maxAge': '10', 'adultAge': '1', 'sex': ['Hermaphrodit', ''], 'genom': 'Kobolt/Dämon', 'height': { 'min': '1,10 m', 'max': '1,20 m' },
      'character': ['Feinfühlig', 'Unterwürfig'], 'raceskill': 'Servicemitarbeiter', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Lecker, oft als Sklaven '
    },
    'Octarie': {
      'maxAge': '30', 'adultAge': '6', 'sex': ['Metamorph', ''], 'genom': 'Oktopode/Insekt', 'height': { 'min': ',80 m', 'max': '1,60 m' },
      'character': ['Einfühlsam', 'Unentschlossen'], 'raceskill': 'Telepathie', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Knorpelwesen, großteils im Wasser, Mehrgliedrig'
    },
    'Fortaner': {
      'maxAge': '180', 'adultAge': '10', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Echse', 'height': { 'min': '1,70 m', 'max': '2,40 m' },
      'character': ['Zuverlässig', 'Grobmotorisch'], 'raceskill': 'Stärke', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Waran-ähnlich, Kraftsportbegeistert, Volkssport: Felsweitwurf, hohe ravitation gewohnt'
    },
    'Benchy': {
      'maxAge': 'keine', 'adultAge': '14', 'sex': ['Weiblich', ''], 'genom': 'Mutant/Humanoid', 'height': { 'min': '1,60m', 'max': '1,80m' },
      'character': ['Geheimnisvoll', 'Unheimlich'], 'raceskill': 'Feldharmonik', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Küstlich, menschliches Aussehen, Cry of the Banshee, Schwingunsharmonie als Relegion'
    },
    'Botana': {
      'maxAge': '840', 'adultAge': '6', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Pflanze', 'height': { 'min': ',80 m', 'max': '2,40 m' },
      'character': ['Robust', 'Pazifistisch'], 'raceskill': 'Petmaster', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Laufende Holzpuppe mit Blättern, "Dryade / Ent"'
    },
    'Firn': {
      'maxAge': '300', 'adultAge': '13', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Kobold/Insekt', 'height': { 'min': ',30 m', 'max': ',50 m' },
      'character': ['Geschickt', 'Aufdringlich'], 'raceskill': 'Feinmechanik', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': '"Fee/Pixie", Naturverbunden'
    },
    'Drakonier': {
      'maxAge': '5000', 'adultAge': '100', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Dämon/Echse', 'height': { 'min': '2,50 m', 'max': '5,0 m' },
      'character': ['Langlebig', 'Faul'], 'raceskill': 'Raumtauglich', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': '2 Fülgel, 4 Beine, vorderbeine als Hände, "Drache"'
    },
    'Unda [Nad-Nad]': {
      'maxAge': '25', 'adultAge': '2', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Nagetier/Kobold', 'height': { 'min': '1,20 m', 'max': '1,50 m' },
      'character': ['Gesellig', 'ungepflegt'], 'raceskill': 'Bergarbieter', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Rattenähnlich, teilweise behaart, viele Nachkommen, leben gern unterirdisch'
    },
    'Quiril [Nad-Nad]': {
      'maxAge': '30', 'adultAge': '3', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Nagetier/Kobold', 'height': { 'min': '1,40 m', 'max': '1,70 m' },
      'character': ['Gutmütig', 'Unzüchtig'], 'raceskill': 'Landwirt', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"Streifenhörnchen", priorisiert Nüsse'
    },
    'Inua [Nad--Nad]': {
      'maxAge': '30', 'adultAge': '3', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Nagetier/Kobold', 'height': { 'min': '1,30 m', 'max': '1,60 m' },
      'character': ['Resistent (kälte)', 'Langschläfer'], 'raceskill': 'Jäger', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'dichtes Fell, Wieselartig, gute Wahrnehmung, '
    },
    'Gruvar': {
      'maxAge': '300', 'adultAge': '20', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Humanoid/Kobold', 'height': { 'min': '1,20 m', 'max': '1,40 m' },
      'character': ['Robust', 'Dickköpfig'], 'raceskill': 'Mechaniker', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"Zwerg"'
    },
    'Pluta': {
      'maxAge': '300', 'adultAge': '20', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Kobold/Mutant', 'height': { 'min': '1,20 m', 'max': '1,40 m' },
      'character': ['Inteligent', 'Träge'], 'raceskill': 'Geologe', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"Zwerg aus Metall" -> Haut hat Strahlungsschutz'
    },
    'Goyle': {
      'maxAge': 'keine', 'adultAge': '100', 'sex': ['Metamorph', ''], 'genom': 'Dämon/Mutant', 'height': { 'min': '1,20 m', 'max': '3,0 m' },
      'character': ['Intelligent', 'Unflexibel'], 'raceskill': 'Informationsbeschaffung', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': '"Gargoyle", fallen in Sonnenschlaf (werden zu Stein), Sitzen zum beobachten gern versteinert auf Gebäuden'
    },
    'Asari': {
      'maxAge': '430', 'adultAge': '20', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Oktopode/Insekt', 'height': { 'min': '1,70 m', 'max': '1,90 m' },
      'character': ['Gutmütig', 'Eitel'], 'raceskill': 'Heilkunde', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': '"sind Amphibisch, sehr schlank und groß gewachsen"'
    },
    'Trix': {
      'maxAge': 'keine', 'adultAge': 'nie', 'sex': ['Neutrum', ''], 'genom': 'Maschine/Primat', 'height': { 'min': '1,20 m', 'max': '1,20 m' },
      'character': ['Friedlich', 'Gutgläubig'], 'raceskill': 'Effizenz', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Kleine Roboäffchen, gern als Werkzeug genutzt, Massengefertigt, Displaygesicht'
    },
    'Mecraidt': {
      'maxAge': '2500', 'adultAge': 'nie', 'sex': ['Neutrum', ''], 'genom': 'Maschine/Humanoid', 'height': { 'min': ',80 m', 'max': '2,40 m' },
      'character': ['Langlebig', 'Hivebewustsein'], 'raceskill': 'Verwaltung', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': 'Variables Aussehen, Lethargisch wenn allein'
    },
    'Far': {
      'maxAge': 'keine', 'adultAge': 'nie', 'sex': ['Neutrum', ''], 'genom': 'Maschiene', 'height': { 'min': '2,0 m', 'max': '5,0 m' },
      'character': ['Xenophil', 'Unkoordiniert'], 'raceskill': 'Erkundung', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': 'Erkundungsdronen (Schiff - DS-SS ) ausgestorbener Erbauer'
    },
    '08/15': {
      'maxAge': '8', 'adultAge': 'nie', 'sex': ['Neutrum', ''], 'genom': 'Maschiene', 'height': { 'min': '1,80 m', 'max': '1,80 m' },
      'character': ['Gehorsam', 'Unbeliebt'], 'raceskill': 'Arbeiter', 'canFly': 1, 'playerRace': 1, 'companionRace': 0, 'desc': 'Menschenförmige Androiden von PsychoDim'
    },
    'Grustelzicke': {
      'maxAge': '6', 'adultAge': '1', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Ziege', 'height': { 'min': ',60 m', 'max': '1,10 m' },
      'character': ['Zahm', 'Dickköpfig'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Schattenwolf': {
      'maxAge': 'keine', 'adultAge': 'nie', 'sex': ['Metamorph', ''], 'genom': 'Dämon/Wolf', 'height': { 'min': ',50 m', 'max': ',85 m' },
      'character': ['Treu', 'Teritorial'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Jivagochi': {
      'maxAge': 'keine', 'adultAge': 'nie', 'sex': ['Neutrum', ''], 'genom': 'Echse/Maschine', 'height': { 'min': ',20 m', 'max': '1,20 m' },
      'character': ['Zahm', 'Lästig'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Brumschaf': {
      'maxAge': '4', 'adultAge': '1', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Schaf', 'height': { 'min': ',60 m', 'max': '1,30 m' },
      'character': ['Zahm', 'Wehrlos'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Aura-Nerzel': {
      'maxAge': '10', 'adultAge': '1', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Nagetier', 'height': { 'min': ',30 m', 'max': ',50 m' },
      'character': ['Friedlich', 'Verfressen'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Pferd': {
      'maxAge': '', 'adultAge': '', 'sex': ['', ''], 'genom': '', 'height': { 'min': '', 'max': '' },
      'character': ['', ''], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Wildschwein': {
      'maxAge': '15', 'adultAge': '2', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Schwein', 'height': { 'min': '0,9m', 'max': '2,2m' },
      'character': ['Spührnase', 'Tempramentvoll'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Amarie': {
      'maxAge': 'keine', 'adultAge': 'nie', 'sex': ['Neutrum', ''], 'genom': 'Maschine (R-schiff)', 'height': { 'min': '5,00 m', 'max': '20,00 m' },
      'character': ['Analytisch', 'Humorlos'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Relik': {
      'maxAge': 'keine', 'adultAge': '160', 'sex': ['Metamorph', ''], 'genom': 'Lipoith (R-schiff)', 'height': { 'min': '80,00 m', 'max': '10 km' },
      'character': ['Geduldig', 'Extrem langsam'], 'raceskill': '', 'canFly': 0, 'playerRace': 0, 'companionRace': 0, 'desc': ''
    },
    'Lyxaria': {
      'maxAge': '620', 'adultAge': '12', 'sex': ['Männlich', 'Weiblich'], 'genom': 'Katze/Dämon', 'height': { 'min': '1,50m', 'max': '2,00m' },
      'character': ['Neugirig', 'Unzüchtig'], 'raceskill': '', 'canFly': 0, 'playerRace': 1, 'companionRace': 0, 'desc': ''
    }
});

ORBITAL.effects = 
[
  /****/
  {
    id: "orbital.dead",
    name: "orbital.effects.status.dead",
    img: "systems/orbital/assets/effects/skull-crossed-bones.svg"
  },
  {
    id: "orbital.unconscious",
    name: "orbital.effects.status.unconscious",
    img: "systems/orbital/assets/effects/knockout.svg"
  },
  {
    id: "orbital.incapacitated",
    name: "orbital.effects.status.incapacitated",
    img: "systems/orbital/assets/effects/handcuffed.svg"
  },
  {
    id: "orbital.battered",
    name: "orbital.effects.status.battered",
    img: "systems/orbital/assets/effects/arm-sling.svg"
  },
  /****/
  {
    id: "orbital.marked",
    name: "orbital.effects.status.marked",
    img: "systems/orbital/assets/effects/arrow-scope.svg"
  },
  {
    id: "orbital.visiondown",
    name: "orbital.effects.status.visiondown",
    img: "systems/orbital/assets/effects/blindfold.svg"
  },
  {
    id: "orbital.visionup",
    name: "orbital.effects.status.visionup",
    img: "systems/orbital/assets/effects/eye-target.svg"
  },
  {
    id: "orbital.invisible",
    name: "orbital.effects.status.invisible",
    img: "systems/orbital/assets/effects/invisible.svg"
  },
  /****/
  {
    id: "orbital.hidden",
    name: "orbital.effects.status.hidden",
    img: "systems/orbital/assets/effects/spy.svg"
  },
  {
    id: "orbital.elevated",
    name: "orbital.effects.status.elevated",
    img: "systems/orbital/assets/effects/tree-growth.svg"
  },
  {
    id: "orbital.halfcover",
    name: "orbital.effects.status.halfcover",
    img: "systems/orbital/assets/effects/armor-downgrade.svg"
  },
  {
    id: "orbital.fullcover",
    name: "orbital.effects.status.fullcover",
    img: "systems/orbital/assets/effects/armor-upgrade.svg"
  },
  /****/
  {
    id: "orbital.restrained",
    name: "orbital.effects.status.restrained",
    img: "systems/orbital/assets/effects/sticky-boot.svg"
  },
  {
    id: "orbital.fear",
    name: "orbital.effects.status.fear",
    img: "systems/orbital/assets/effects/ghost.svg"
  },
  {
    id: "orbital.exhausted",
    name: "orbital.effects.status.exhausted",
    img: "systems/orbital/assets/effects/heart-beats.svg"
  },
  {
    id: "orbital.asleep",
    name: "orbital.effects.status.asleep",
    img: "systems/orbital/assets/effects/pillow.svg"
  },
  /****/
  {
    id: "orbital.burning",
    name: "orbital.effects.status.burning",
    img: "systems/orbital/assets/effects/fire-silhouette.svg"
  },
  {
    id: "orbital.poisoned",
    name: "orbital.effects.status.poisoned",
    img: "systems/orbital/assets/effects/poison-bottle.svg"
  },
  {
    id: "orbital.downgrade",
    name: "orbital.effects.status.downgrade",
    img: "systems/orbital/assets/effects/sergeant-rotated.svg"
  },
  {
    id: "orbital.upgrade",
    name: "orbital.effects.status.upgrade",
    img: "systems/orbital/assets/effects/sergeant.svg"
  },
  /****/
  {
    id: "orbital.silenced",
    name: "orbital.effects.status.silenced",
    img: "systems/orbital/assets/effects/silence.svg"
  },
  {
    id: "orbital.commanded",
    name: "orbital.effects.status.commanded",
    img: "systems/orbital/assets/effects/rally-the-troops.svg"
  },
  {
    id: "orbital.manipulated",
    name: "orbital.effects.status.manipulated",
    img: "systems/orbital/assets/effects/gingerbread-man.svg"
  },
  {
    id: "orbital.flying",
    name: "orbital.effects.status.flying",
    img: "systems/orbital/assets/effects/hand-wing.svg"
  },
  /**Dice Efffects */
  {
    id: "orbital.roll.up.1",
    name: "orbital.effects.status.roll.up.1",
    img: "systems/orbital/assets/effects/up1.svg"
  },
  {
    id: "orbital.roll.up.2",
    name: "orbital.effects.status.roll.up.2",
    img: "systems/orbital/assets/effects/up2.svg"
  },
  {
    id: "orbital.roll.up.4",
    name: "orbital.effects.status.roll.up.4",
    img: "systems/orbital/assets/effects/up4.svg"
  },
  {
    id: "orbital.roll.up.6",
    name: "orbital.effects.status.roll.up.6",
    img: "systems/orbital/assets/effects/up6.svg"
  },
  /****/
  {
    id: "orbital.roll.down.1",
    name: "orbital.effects.status.roll.down.1",
    img: "systems/orbital/assets/effects/down1.svg"
  },
  {
    id: "orbital.roll.down.2",
    name: "orbital.effects.status.roll.down.2",
    img: "systems/orbital/assets/effects/down2.svg"
  },
  {
    id: "orbital.roll.down.4",
    name: "orbital.effects.status.roll.down.4",
    img: "systems/orbital/assets/effects/down4.svg"
  },
  {
    id: "orbital.roll.down.6",
    name: "orbital.effects.status.roll.down.6",
    img: "systems/orbital/assets/effects/down6.svg"
  },
];

ORBITAL.specialEffects = { DEFEATED: "orbital.dead", UNCONSCIOUS: "orbital.unconscious", INVISIBLE: "orbital.invisible", BLIND: "orbital.visiondown", FLY: "orbital.flying"};