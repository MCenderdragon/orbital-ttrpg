

export class WurfelBase
{
	formula = "d20"

	constructor(formula)
	{
		this.formula = formula
	}
	
	get tooltip()
	{

	}

	getResult()
	{

	}

	asJson()
	{

	}
}

export class Wurfel2D20 extends WurfelBase
{
	modifiers = []
	named_modifiers = {}
	#finalModifier = false
	#dieA = false
	#dieB = false
	#roll = false

	constructor()
	{
		super("2D20");
	}

	addModifier(name, value, key=false)
	{
		this.finalModifier = false;
		this.dieA = false
		this.dieB = false
		this.roll = false

		const modEntry = {"name":name, "value": value}

		//console.log(this.modifiers, this.named_modifiers)
		if(key)
		{
			this.named_modifiers[key] = modEntry;
		}
		else
		{
			return this.modifiers.push(modEntry);
		}
	}

	addModifierFromActor(actor)
	{
		let bonusMalus = 0;
		let values = [1,2,4,6];
		for (const val of values)
		{
			if(actor.statuses.has(`orbital.roll.up.${val}`))
				bonusMalus += val;
			if(actor.statuses.has(`orbital.roll.down.${val}`))
				bonusMalus -= val;
		}
		if(bonusMalus!=0)
			this.addModifier(bonusMalus>0 ? "bonus" : "malus", bonusMalus, "actor_bonus_malus");
	}

	get tooltip()
	{
		let str = "";
		this.modifiers.forEach( (e,index) => {
			//console.log("tooltip 1", e, index);
			str += `${e.name}:${e.value} `
		});
		for(let key in this.named_modifiers)
		{
			const e = this.named_modifiers[key];
			//console.log("tooltip 2", e, key);
			str += `${e.name}:${e.value} `
		}
		return (str.length > 0) ? `${this.calcFinalModifier()} (${str})` : "0";
	}

	calcFinalModifier()
	{
		if(this.finalModifier)
			return this.finalModifier;
		else
		{
			this.finalModifier = 0;
			this.modifiers.forEach( (e,index) => {
				this.finalModifier += e.value
			});
			for(let key in this.named_modifiers)
			{
				const e = this.named_modifiers[key];
				this.finalModifier += e.value
			}
			return this.finalModifier;
		}
	}

	get min()
	{
		return 21 - this.calcFinalModifier();
	}

	get max()
	{
		return 21 + this.calcFinalModifier();
	}

	asJson()
	{
		return {
			"min": this.min,
			"max": this.max,
			"tooltip": this.tooltip,
			"dice": this
		};
	}

	async getResult(actor)
	{
		if(!roll)
		{
			roll = new Roll("2d20", actor.getRollData());
			await roll.roll();
			//console.log(roll);
		}
		if(!this.dieA || !this.dieB)
		{
			const dieA = roll.terms[0].results[0].result;
			const dieB = roll.terms[0].results[1].result;
		}
		let text = "göl";
		if(dieA == dieB)//krit success
		{
			text = "krit. Erfolg";
		}
		else if( (dieA == 20) || (dieB == 20))
		{
			text = "krit. Fail";
		}
		else if((this.min) <= (dieA+dieB) && (dieA+dieB) <= (this.max))
		{
			text = "Erfolg";
		}
		else
		{
			text = "Fail";
		}
		return text;
	}

	postResult(actor, rollType, skillVal, skillAtr)
	{
		let label = `[${rollType}] ${skillVal} ${skillAtr} (${this.min} - ${this.max})` + "<br>"+this.tooltip;
		
		roll.toMessage({
			user: game.user.id,
			speaker: ChatMessage.getSpeaker({ actor: actor }),
			flavor: label,
			rollMode: game.settings.get('core', 'rollMode')
		});

		let content = `<div>${this.getResult()}</div>`
		ChatMessage.create({content:content, speaker: ChatMessage.getSpeaker({ actor: actor })});
	}
}