
import { OrbitalActor } from '../documents/actor.mjs';
import { Wurfel2D20 } from './wurfel.mjs';

function _getMainHand(actor)
{
	return actor.getItem("mainhand");
}

function _getOffHand(actor)
{
	return actor.getItem("offhand");
}

function _getSafe(item, systemName, defaultVal)
{
	if(item)
	{
		return item.system[systemName];
	}
	return defaultVal;
}

export function booleanFix(val)
{
	return val == "true" || val === true;
}

export function getSkill(actor, skillName, subskillName = undefined)
{
	if(actor)
	{
		for (let [key, skill] of Object.entries(actor.system.skills))
		{
			if(skill.value === skillName)
			{
				if(subskillName === undefined)
					return skill;
				else if(skill.subskill === subskillName)
				{
					return skill;
				}
			}
		}
	}
	return {level: 0, subskill: "", attribute: "", value: "", wurfel: {min: 21, max:21}};
}

/**
 * 
 * @param {OrbitalActor} actor 
 * @param {string} attribute 
 * @returns 
 */
export function getAttributeStufe(actor, attribute)
{
	if(actor)
	{
		return actor._getAttributeFromActor(attribute);
	}
	return 0;
}

export function getSkillLevel(actor, skillName, subskillName = undefined)
{
	const s = getSkill(actor, skillName, subskillName);
	console.log(s)
	if(s)
	{
		return s.level;
	}
	return 0;
}

function i18nSkill(skillName)
{
	return game.i18n.localize("orbital.skill."+skillName);
}

function i18nSubskill(skillName)
{
	return game.i18n.localize("orbital.subskill."+skillName);
}

/**
 * 
 * @param {OrbitalActor} actor 
 * @returns {Object} "ATK": damage_lethal, "BET": damage_stun, "chance": attack_chance, "wurfel": wurfel.asJson();
 */
export function calcAttack(actor)
{
	const isHidden = actor.statuses.has('invisible');
	const fullCover = actor.statuses.has("orbital.fullcover");

	const itemA = _getMainHand(actor);
	const itemB = _getOffHand(actor);
	const Typ_Hand = _getSafe(itemA, "attribute", "");
	const Typ_Andere_Hand = _getSafe(itemB, "attribute", "");
	const isGunA = booleanFix(_getSafe(itemA, "is_gun", false));
	const isGunB = booleanFix(_getSafe(itemB, "is_gun", false));

	const sameType = Typ_Hand == Typ_Andere_Hand;

	//item damage is fixed, and all special are already applied (like AUD Weapons having more damage)
	const damage_stunA = _getSafe(itemA, "damage_stun", 0);
	const damage_lethalA = _getSafe(itemA, "damage_lethal", 0);
	const damage_stunB = _getSafe(itemB, "damage_stun", 0);
	const damage_lethalB = _getSafe(itemB, "damage_lethal", 0);

	const stufeItemB = _getSafe(itemB, "stufe", 0);

	const skilAName = isGunA ? "Schütze" : "Nahkämpfer"
	const skillA = getSkill(actor, skilAName);

	let damage_stun = getAttributeStufe(actor, "STR");
	let damage_lethal = Math.ceil(damage_stun / 2.0);
	let attack_chance = getAttributeStufe(actor, "INT");
	let tooltip = "";

	console.log("Hand", Typ_Hand)
	console.log("Item", itemA, itemB)

	const wurfel = new Wurfel2D20();

	if(itemA || itemB)
	{
		switch(Typ_Hand)
		{
			case "GES":
				damage_stun = damage_stunA + Math.ceil(damage_stunB/2.0);
				damage_lethal = damage_lethalA + Math.ceil(damage_lethalB/2.0);
				tooltip = `${Typ_Hand}:${getAttributeStufe(actor, Typ_Hand)} + ${skilAName}:${skillA.level}`;
				attack_chance = getAttributeStufe(actor, Typ_Hand) + skillA.level;
				wurfel.addModifier(Typ_Hand, getAttributeStufe(actor, Typ_Hand));
				wurfel.addModifier(i18nSkill(skilAName), skillA.level);
				//console.log("attack_chance", attack_chance, getAttributeStufe(actor, "GES"), skillA.level, skillA, isGunA)
				break;
			case "REF":
				damage_stun = damage_stunA;
				damage_lethal = damage_lethalA;

				if(!isGunA)
				{
					if(isHidden)
					{	
						const factor = (getSkillLevel(actor, "Nahkämpfer", "Attentäter") > 0) ? 4 : 3;
						damage_stun = damage_stunA * factor;
						damage_lethal = damage_lethalA * factor;
					}
				}
				else
				{
					if(getSkillLevel(actor, "Schütze", "Kommando"))
					{
						damage_stun += 3;
						damage_lethal += 2;
					}
					if(isHidden)
					{
						const factor = 2;
						damage_stun *= factor;
						damage_lethal *= factor;
						
					}		
				}
				tooltip = `${Typ_Hand}:${getAttributeStufe(actor, Typ_Hand)} + ${skilAName}:${skillA.level}`;
				attack_chance = getAttributeStufe(actor, Typ_Hand) + skillA.level;
				wurfel.addModifier(Typ_Hand, getAttributeStufe(actor, Typ_Hand));
				wurfel.addModifier(i18nSkill(skilAName), skillA.level);
				break;
			case "AUD":
				attack_chance = getAttributeStufe(actor, "AUD"); //atk berechnung ist im Item
				tooltip = `${Typ_Hand}:${getAttributeStufe(actor, "AUD")}`;
				wurfel.addModifier(Typ_Hand, getAttributeStufe(actor, Typ_Hand));

				damage_stun = damage_stunA;
				damage_lethal = damage_lethalA;
				break;
			case "PEZ":
				if(!isGunA)
				{
					damage_stun = damage_stunA;
					damage_lethal = damage_lethalA;
					attack_chance = getAttributeStufe(actor, "PEZ") + skillA.level + stufeItemB;
					tooltip = `${Typ_Hand}:${getAttributeStufe(actor, "PEZ")} + ${skilAName}:${skillA.level} + Offhand:${stufeItemB}`;
					wurfel.addModifier(Typ_Hand, getAttributeStufe(actor, Typ_Hand));
					wurfel.addModifier(i18nSkill(skilAName), skillA.level);
					wurfel.addModifier("Offhand", stufeItemB)
				}
				else
				{
					damage_stun = damage_stunA;//more damage in Item
					damage_lethal = damage_lethalA;
					tooltip = `${Typ_Hand}:${getAttributeStufe(actor, Typ_Hand)} + ${skilAName}:${skillA.level}`;
					attack_chance = getAttributeStufe(actor, Typ_Hand) + skillA.level;
					wurfel.addModifier(Typ_Hand, getAttributeStufe(actor, Typ_Hand));
					wurfel.addModifier(i18nSkill(skilAName), skillA.level);
				}
				break;
			default:
				damage_stun = damage_stunA;
				damage_lethal = damage_lethalA;
				attack_chance = getAttributeStufe(actor, Typ_Hand) + skillA.level;
				tooltip = `${Typ_Hand}:${getAttributeStufe(actor, Typ_Hand)} + ${skilAName}:${skillA.level}`;
				wurfel.addModifier(Typ_Hand, getAttributeStufe(actor, Typ_Hand));
				wurfel.addModifier(i18nSkill(skilAName), skillA.level);
				break;
		}
	}

	//spezialsiierung passt
	if(skillA.attribute === Typ_Hand && skillA.subskill != "Keine")
	{
		attack_chance += 2;
		wurfel.addModifier(i18nSubskill(skillA.subskill), 2);
	}

	if(fullCover)
	{
		attack_chance -= 2;
		wurfel.addModifier("Full Cover", -2);
	}

	wurfel.addModifierFromActor(actor);
	return {"ATK": damage_lethal, "BET": damage_stun, "chance": attack_chance, "wurfel": wurfel.asJson()};
}
/**
 * 
 * @param {OrbitalActor} actor 
 * @returns int (0 - 4)
 */
function getCoverBonus(actor)
{
	if(actor)
	{
		const halfCover = actor.statuses.has("orbital.halfcover");
		const fullCover = actor.statuses.has("orbital.fullcover");

		if(fullCover)
		{
			return 4;
		}
		else if(halfCover)
		{
			return 2;
		}
	}
	return 0;
}


/**
 * 
 * @param {OrbitalActor} actor 
 * @returns {Object} "shield_phys": armorPhys+finalArmorValueP,
 * 					 "shield_mind": armorMind+finalArmorValueM,
 * 					 "chances": {"close": {"chance": 1,"wurfel": ...}, "ranged": ..., "magic": ...};
 */
export function calcArmor(actor, apply_armor=true, apply_shield=true)
{
	
	console.log("calcArmor for", actor, apply_armor, apply_shield);

	const itemA = _getMainHand(actor);
	const itemB = _getOffHand(actor);
	const Typ_Hand = _getSafe(itemA, "attribute", "");
	const Typ_Andere_Hand = _getSafe(itemB, "attribute", "");
	const isShieldA = booleanFix(_getSafe(itemA, "is_shield", false));
	const isShieldB = booleanFix(_getSafe(itemB, "is_shield", false));

	const stufeItemA = _getSafe(itemA, "stufe", 0);
	const stufeItemB = _getSafe(itemB, "stufe", 0);

	let armorPhys = 0;
	let armorMind = 0;
	let hasShield = false;

	if(booleanFix(apply_shield))
	{
		if(!isShieldA && isShieldB)
		{
			hasShield = true;
			//swap variables so the SHield has also full effec tin off hand.
			if(Typ_Andere_Hand == "RES")
			{
				armorPhys = stufeItemB;
				armorMind = Math.ceil(stufeItemB / 2);
			}
			else
			{
				armorPhys = Math.ceil(stufeItemB / 2);
				armorMind = stufeItemB;
			}
		}
		else
		{
			if(isShieldA)
			{
				hasShield = true;
				if(Typ_Hand == "RES")
				{
					armorPhys = stufeItemA;
					armorMind = Math.ceil(stufeItemA / 2);
				}
				else
				{
					armorPhys = Math.ceil(stufeItemA / 2);
					armorMind = stufeItemA;
				}
			}
			if(isShieldB)
			{
				hasShield = true;
				//Keine Auswertung der Anderen Hand!!!
				if(Typ_Andere_Hand == "RES")
				{
					armorPhys += Math.ceil(stufeItemB/2);
					armorMind += Math.ceil(Math.ceil(stufeItemB/2) / 2);
				}
				else
				{
					armorPhys += Math.ceil(Math.ceil(stufeItemB/2) / 2);
					armorMind += Math.ceil(stufeItemB/2);
				}
			}
		}	
	}

	const armorValuesP = {};
	const armorValuesM = {};

	actor.items.filter((i) => i.isEquipped() && (i.type == "armor") && (CONFIG.ORBITAL.armor_phys.includes(i.system.attribute))).forEach((i) => armorValuesP[i.system.slot] = (armorValuesP[i.system.slot] || 0) + i.system.armor.value );
	actor.items.filter((i) => i.isEquipped() && (i.type == "armor") && (CONFIG.ORBITAL.armor_mind.includes(i.system.attribute))).forEach((i) => armorValuesM[i.system.slot] = (armorValuesM[i.system.slot] || 0) + i.system.armor.value );
	
	let finalArmorValueP = 0;
	let finalArmorValueM = 0;
	console.log("finalArmorValueP",  apply_armor, finalArmorValueP, finalArmorValueM);
	if(booleanFix(apply_armor))
	{
		for (let [slot, entries] of Object.entries(CONFIG.ORBITAL.equipment_slots))
		{
			if(entries.isArmor === true)
			{
				if(armorValuesP[slot])
				{
					finalArmorValueP += armorValuesP[slot];
				}
				if(armorValuesM[slot])
				{
					finalArmorValueM += armorValuesM[slot];
				}
			}
		}
	}
	console.log("finalArmorValueP",  finalArmorValueP, finalArmorValueM);

	const defendChances =  {"close": 0, "ranged": 0, "magic": 0};
	const defendChancesW =  {"close": new Wurfel2D20(), "ranged": new Wurfel2D20(), "magic": new Wurfel2D20()};

	let canBlock = {"close": false, "ranged": false, "magic": false};
	if(itemA)
	{
		const block = itemA.canBlock();
		canBlock.close |= block.close;
		canBlock.ranged |= block.ranged;
		canBlock.magic |= block.magic;
	}
	if(itemB)
	{
		const block = itemB.canBlock();
		canBlock.close |= block.close;
		canBlock.ranged |= block.ranged;
		canBlock.magic |= block.magic;
	}

	//close combat
	{
		let evadeChance = getAttributeStufe(actor, "REF");

		if( getSkillLevel(actor, "Athlet", "Ausweichen") > 0)
		{
			evadeChance += 2 + getSkillLevel(actor, "Athlet");
		}

		const justWeaponBlock = (!hasShield && canBlock.close) ? getSkillLevel(actor, "Nahkämpfer") : 0;
		const closeShieldBlock = (hasShield && canBlock.close) ? (getAttributeStufe(actor, "RES") + getSkillLevel(actor, "Verteidiger")) : 0;

		defendChances.close = Math.max(evadeChance, justWeaponBlock, closeShieldBlock);
		if(defendChances.close == evadeChance)
		{
			defendChancesW.close.addModifier("REF", getAttributeStufe(actor, "REF"));
			if( getSkillLevel(actor, "Athlet", "Ausweichen") > 0)
			{
				defendChancesW.close.addModifier(i18nSkill("Athlet"), getSkillLevel(actor, "Athlet"));
				defendChancesW.close.addModifier(i18nSubskill("Ausweichen"), 2);
			}
		}
		else if(defendChances.close == justWeaponBlock)
		{
			defendChancesW.close.addModifier(i18nSkill("Nahkämpfer"), getSkillLevel(actor, "Nahkämpfer"));
		}
		else if(defendChances.close == closeShieldBlock)
		{
			defendChancesW.close.addModifier("RES", getAttributeStufe(actor, "RES"));
			defendChancesW.close.addModifier(i18nSkill("Verteidiger"),  getSkillLevel(actor, "Verteidiger"));
		}
	}
	//ranged
	{
		let block = getAttributeStufe(actor, "RES");
		defendChancesW.ranged.addModifier("RES", getAttributeStufe(actor, "RES"));
		if(hasShield && canBlock.ranged)
		{
			block += + getSkillLevel(actor, "Verteidiger");
			defendChancesW.ranged.addModifier(i18nSkill("Verteidiger"), getSkillLevel(actor, "Verteidiger"));
		}
		block += getCoverBonus(actor);
		defendChancesW.ranged.addModifier("Deckung", getCoverBonus(actor));
		defendChances.ranged = block;
	}
	//magic
	{
		let block = getAttributeStufe(actor, "WIL");
		defendChancesW.magic.addModifier("WIL", getAttributeStufe(actor, "WIL"));
		if(hasShield && canBlock.magic)
		{
			block += + getSkillLevel(actor, "Verteidiger");
			defendChancesW.magic.addModifier(i18nSkill("Verteidiger"), getSkillLevel(actor, "Verteidiger"));
		}
		defendChances.magic = block;
	}

	let chances = {};

	Object.keys(defendChances).forEach(k => {
		chances[k] = {
			"chance": defendChances[k],
			"wurfel": defendChancesW[k].asJson()
		}
	})


	const DEF = {shield_phys: armorPhys+finalArmorValueP, shield_mind: armorMind+finalArmorValueM, "chances": chances};
	console.log("result", DEF,  armorPhys, finalArmorValueP, armorMind, finalArmorValueM);
	return DEF;
}


export function calculateItemAttackChancesForDistance(item, distance)
{
	//in meter
	const schwert = 3; //1 Feld
	const pistole = 45; //15
	const sniper = pistole * 4;//180

	let deltaChance = -50;//default is impossible
	let selfAttackRate = 0;
	let combatType = "none";
	if(item)
	{
		const weaponAttr = item.system.attribute;
		if(distance <= sniper)
		{
			combatType = "longdistance";
			
			if(distance <= pistole)
			{
				combatType = "ranged"
				if(distance <= schwert)
				{
					combatType = "close";
				}
			}	
			if(booleanFix(item.system.is_gun))
			{
				//its a gun
				if(combatType == "ranged")
				{
					if((weaponAttr == "PEZ"))
					{
						deltaChance = -2;
					}
					else
					{
						deltaChance = 0;
					}
				}
				if(combatType == "longdistance")
				{
					if( (weaponAttr == "STR") || (weaponAttr == "REF") || (weaponAttr == "AUD") || (weaponAttr == "AUR"))
					{
						deltaChance = -2;
					}
					else if(weaponAttr == "GES")
					{
						deltaChance = -4;
					}
					else if( (weaponAttr == "INT") || (weaponAttr == "PEZ"))
					{
						//PEZ is sniper and made for this
						//INT is special as weapon and user position are different -> Gamemaster needs to check this.
						deltaChance = 0;
					}
				}
				else if(combatType == "close")
				{	
					//AUD,PEZ,AUR impossible

					if(weaponAttr == "STR")
					{
						//TODO: STR halber ATK an selbst
						deltaChance = 0;
						selfAttackRate = 0.5;
					}
					else if((weaponAttr == "GES") || (weaponAttr == "REF"))
					{
						deltaChance = -2;
					}
					else if(weaponAttr == "INT")
					{
						deltaChance = 0;
					}
				}
			}
			else //weapon is made for close combat aka swords
			{
				
				if(combatType == "ranged")
				{
					if(weaponAttr == "AUR")
					{
						deltaChance = -4;
					}
					else if(weaponAttr == "INT") //e.g. Whips
					{
						deltaChance = -2;
					}
				}
				else if(combatType == "close")
				{
					deltaChance = 0;
				}
			}
		}
	}
	else //Fist fight!
	{
		deltaChance = 0;//default is impossible
		selfAttackRate = 0;
		combatType = "close";
	}
	return {"item":item, "deltaChance": deltaChance, "selfAttackRate": selfAttackRate, "combatType": combatType};
	
}

/**
 * @param {Actor} actor the actor doing the attack
 * @param {TokenDoc} actorTokenDoc the TokenDoc of this actor 
 * @param {Object} target  the target object consisiting of target.distance and target.token
 */
export function attackTargetToken(actor, actorTokenDoc, target)
{
	const chanceA = calculateItemAttackChancesForDistance(_getMainHand(actor), target.distance);
	const chanceB = calculateItemAttackChancesForDistance(_getOffHand(actor), target.distance);

	console.log(target, chanceA, chanceB);
}

/**
 * 
 * @param {Object} attackResult 
 * @param {Object} defendResult 
 * @return {Array<Object>} 
 */
export function getDamageLogic(attackerDoc, targetDoc, attackType, attackResult, defendResult)
{
	if(defendResult.success && defendResult.krit && !(attackResult.success && attackResult.krit))
	{
		//counter
		ChatMessage.create({
			content: `
			<div>
				Counter! ${targetDoc?.name} attacks ${attackerDoc?.name}
			</div>
			`,
			speaker: ChatMessage.getSpeaker({ actor: targetDoc?.actor })
		});
		return [];
	}
	else if(!attackResult.success && !attackResult.krit)
	{
		return [];
	}
	else if(attackResult.success)
	{
		let dmgDice = attackResult.krit ? "1d6" : "1d4";
		let applyArmor = null;
		let applyShield = null;

		if(attackResult.krit && defendResult.krit && defendResult.success)
		{
			//damage to both
			applyArmor = false;
			applyShield = false;

			return [
				{"attacker":attackerDoc, "target":targetDoc, "attack_type": attackType, "damage_dice": dmgDice, "apply_armor": applyArmor, "apply_shield":applyShield},
				{"attacker":targetDoc, "target":attackerDoc, "attack_type": attackType, "damage_dice": dmgDice, "apply_armor": applyArmor, "apply_shield":applyShield}
			];
		}
		else
		{
			if(defendResult.success)
			{
				applyArmor=true;
				applyShield=true;
			}
			else if(!defendResult.success && defendResult.krit)
			{
				applyArmor = false;
				applyShield = false;
			}
			else if(!defendResult.success && !defendResult.krit)
			{
				applyArmor = false;
				applyShield = true;
			}

			return [{"attacker":attackerDoc, "target":targetDoc, "attack_type": attackType, "damage_dice": dmgDice, "apply_armor": applyArmor, "apply_shield":applyShield}];
		}

	}
	return [];
}

const roll_chances = {};

export function getChanceLvl(lvl)
{
	if(!roll_chances.pre_calc)
	{
		roll_chances.pre_calc = [];
	}
	if(roll_chances.pre_calc[lvl])
	{
		return roll_chances.pre_calc[lvl];
	}
	else
	{
		roll_chances.pre_calc[lvl] = getChanceWurfel(21-lvl, 21+lvl);
		return roll_chances.pre_calc[lvl];
	}
}

export function getChanceWurfel(min, max)
{
	if(!roll_chances.roll_count)
	{
		roll_chances.roll_count = [];
		roll_chances.fail_count = 0;
		roll_chances.krit_count = 0;
		roll_chances.total_count = 20 * 20;
		for (let n = 1; n <= 20 ; n++) 
		{
			for (let m = 1; m <= 20 ; m++) 
			{

				if(n == m)
				{
					roll_chances.krit_count += 1;
				}
				else if(n == 20 || m == 20)
				{
					roll_chances.fail_count += 1;
				}
				else
				{
					if(roll_chances.roll_count[n+m])
					{
						roll_chances.roll_count[n+m] += 1;
					}
					else
					{
						roll_chances.roll_count[n+m] = 1;
					}
				}
			}
		}
	}

	let total = roll_chances.krit_count;
	for(let i=min;i<=max;i++)
	{
		total += roll_chances.roll_count[i];
	}

	return total / roll_chances.total_count;

}