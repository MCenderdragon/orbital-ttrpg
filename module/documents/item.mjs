/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
import { OrbitalActor } from './actor.mjs';
import { getSkillLevel, booleanFix } from '../helpers/combat.mjs';


export class OrbitalItem extends Item {
	/**
	 * Augment the basic Item data model with additional dynamic data.
	 */
	prepareData() {
		// As with the actor class, items are documents that can have their data
		// preparation methods overridden (such as prepareBaseData()).
		super.prepareData();
	}

	

	prepareDerivedData() 
	{
		//console.log("prepareDerivedData", this);  
		const itemData = this;
		const systemData = itemData.system;

		if(itemData.type == "weapon")
		{
			const weaponAttr = systemData.attribute;
			const is_shield = booleanFix(systemData.is_shield);
			if(is_shield)
			{
				systemData.is_gun = false;
			}
			const isGun = booleanFix(systemData.is_gun);
			const weaponConfigList = isGun ? CONFIG.ORBITAL.fernkampf_attribute : CONFIG.ORBITAL.nahkampf_attribute; 

			if(is_shield)
			{
				weaponConfigList["RES"] = {"two_handed": false, "one_handed": true, "schilde": true, "can_block": true};
				weaponConfigList["WIL"] = {"two_handed": false, "one_handed": true, "schilde": true, "can_block": true};
			}

			let two_handed = booleanFix(systemData.two_handed);

			if(weaponConfigList[weaponAttr])
			{
				const weaponConfig = weaponConfigList[weaponAttr];

				if(!weaponConfig.two_handed && two_handed)
				{
					systemData.two_handed = false;
					two_handed = false;
				}
				if(!weaponConfig.one_handed && !two_handed)
				{
					systemData.two_handed = true;
					two_handed = true;
				}
			}
			else
			{
				systemData.stufe = 0;
			}

			const stufe = systemData.stufe || 1;
			if(weaponAttr == "AUR")
			{
				systemData.is_deadly = false;
				if(two_handed)
				{
					systemData.damage_lethal = 4;
					systemData.damage_stun = 6;
				}
				else
				{
					systemData.damage_lethal = 2;
					systemData.damage_stun = 4;
				}
			}
			else if(weaponAttr == "AUD")
			{
				const lvl = stufe + getSkillLevel(this.actor, isGun ? "Schütze" : "Nahkämpfer");
				const deadly = booleanFix(systemData.is_deadly);
				if(deadly)
				{
					systemData.damage_lethal = lvl;
					systemData.damage_stun = 0;
				}
				else
				{
					systemData.damage_lethal =  Math.ceil(lvl/2.0);
					systemData.damage_stun = lvl + 2;
				}
			}
			else if(weaponAttr == "PEZ" && isGun)
			{
				const lvl = stufe + (getSkillLevel(this.actor, "Schütze", "Sniper") > 0 ? 4 : 2);
				const deadly = booleanFix(systemData.is_deadly);
				if(deadly)
				{
					systemData.damage_lethal = lvl;
					systemData.damage_stun = 0;
				}
				else
				{
					systemData.damage_lethal =  Math.ceil(lvl/2.0);
					systemData.damage_stun = lvl + 2;
				}
			}
			else
			{
				const deadly = booleanFix(systemData.is_deadly);
				if(deadly)
				{
					systemData.damage_lethal = stufe;
					systemData.damage_stun = 0;
				}
				else
				{
					systemData.damage_lethal =  Math.ceil(stufe/2.0);
					systemData.damage_stun = stufe + 2;
				}
			}

			if(is_shield)
			{
				if(weaponAttr == "RES")
				{
					systemData.damage_lethal = stufe;
					systemData.damage_stun = Math.ceil(stufe / 2);
				}
				else
				{
					systemData.damage_lethal = Math.ceil(stufe / 2);
					systemData.damage_stun = stufe;
				}
			}
		}
		else if(itemData.type == "armor")
		{
			const stufe = systemData.stufe || 1;
			systemData.armor.value = stufe;
		}
		else if(itemData.type == "supportweapon")
		{
			const stufe = systemData.stufe || 1;
			const deadly = systemData.is_deadly===true || systemData.is_deadly == "true";
			if(deadly)
			{
				systemData.damage_lethal = stufe;
				systemData.damage_stun = 0;
			}
			else
			{
				systemData.damage_lethal =  Math.ceil(stufe/2.0);
				systemData.damage_stun = stufe + 2;
			}
		}
		else if(itemData.type == "spell")
		{
			const level = systemData.level.value; //Stufe
			const power = systemData.power.value; //Wert
			if(itemData.effects)
			{
				for (let e of itemData.effects) 
				{
					if(!e.flags["orbital"])
					{
						e.flags["orbital"] = {};
					}
					e.system.level = level;
					e.system.power = power;
				}
			}
		}
	}

	_preCreate(data, options, user)
	{
		super._preCreate(data, options, user);
		if(this.type == "spell")
		{
			this.img = "icons/magic/light/beam-layered-teal.webp";
			this._source.img = this.img;

		}
		else if(this.type == "weapon")
		{
			this.img = "icons/weapons/swords/shortsword-broad.webp";
			this._source.img = this.img;
		}
		else if(this.type == "armor")
		{
			if(this.system.slot == "body")
			{
				this.img = "icons/equipment/chest/breastplate-banded-leather-brown.webp";
			}
			else if(this.system.slot == "head")
			{
				this.img = "icons/equipment/head/cap-simple-leather-brown.webp";
			}
			else if(this.system.slot == "boots")
			{
				this.img = "icons/equipment/feet/boots-leather-simple-brown.webp";
			}
			else if(this.system.slot == "gloves")
			{
				this.img = "icons/equipment/hand/glove-simple-leather-brown-blue.webp";
			}
			else if(this.system.slot == "misc")
			{
				this.img = "icons/equipment/finger/ring-ball-purple.webp";
			}
			
			this._source.img = this.img;
		}
		
		
	}

	/**
	 * Prepare a data object which defines the data schema used by dice roll commands against this Item
	 * @override
	 */
	getRollData() {
		// Starts off by populating the roll data with `this.system`
		const rollData = { ...super.getRollData() };

		// Quit early if there's no parent actor
		if (!this.actor) return rollData;

		// If present, add the actor's roll data
		rollData.actor = this.actor.getRollData();

		return rollData;
	}

	/**
	 * Handle clickable rolls.
	 * @param {Event} event   The originating click event
	 * @private
	 */
	async roll() {
		const item = this;

		// Initialize chat data.
		const speaker = ChatMessage.getSpeaker({ actor: this.actor });
		const rollMode = game.settings.get('core', 'rollMode');
		const label = `[${item.type}] ${item.name}`;

		// If there's no roll data, send a chat message.
		if (!this.system.formula) {
			ChatMessage.create({
				speaker: speaker,
				rollMode: rollMode,
				flavor: label,
				content: item.system.description ?? '',
			});
		}
		// Otherwise, create a roll and send a chat message from it.
		else {
			// Retrieve roll data.
			const rollData = this.getRollData();

			// Invoke the roll and submit it to chat.
			const roll = new Roll(rollData.formula, rollData);
			// If you need to store the value first, uncomment the next line.
			// const result = await roll.evaluate();
			roll.toMessage({
				speaker: speaker,
				rollMode: rollMode,
				flavor: label,
			});
			return roll;
		}
	}

	isEquipped()
	{
		//console.log("isEquipped")
		const itemData = this;
		const systemData = itemData.system;
		if(!this.actor)
			return false;

		if(itemData.type == "spell")
		{
			return true;
		}
		if(itemData.type == "weapon" || itemData.type == "armor")
		{
			if(this.actor instanceof OrbitalActor)
			{
				return this.actor.isEquipped(this);
			}
		}

		return false;
	}

	getEquippedSlot()
	{
		const itemData = this;
		const systemData = itemData.system;
		if(!this.actor)
			return false;

		if(itemData.type == "spell")
		{
			return "spells";
		}
		if(itemData.type == "weapon" || itemData.type == "armor")
		{
			if(this.actor instanceof OrbitalActor)
			{
				if(this.actor.system.inventory)
				{
					const slots = this.getValidSlots();
					for(const slot of slots)
					{
						const items = this.actor.getItem(slot);
						//console.log(slot, "->", items);
						if(Array.isArray(items))
						{
							for(const i of items)
							{
								if(i == this)
									return slot;
							}
						}
						else
						{
							if(items == this)
								return slot;
						}
					}
				}
				return false;
			}
		}

		return false;
	}

	isEquippedIn(slot)
	{
		if(this.actor instanceof OrbitalActor)
		{
			const equippedItem =  this.actor.getItem(slot);
			if(equippedItem && Array.isArray(equippedItem))
			{
				return equippedItem.indexOf(this) >= 0;
			}
			else
			{
				return equippedItem === this;
			}
		}
		return false;
	}

	getValidSlots()
	{
		const systemData = this.system;
		if(this.type == "weapon")
		{
			const two_handed = (systemData.two_handed === true) || (systemData.two_handed == "true");

			if(two_handed) //no offhand
			{
				return ["mainhand"];
			}
			else
			{
				return ["mainhand", "offhand"];
			}
		}
		else if(this.type == "armor")
		{
			return ["head", "body", "boots", "gloves", "misc"];
		}

		return [];
	}
	
	isSlotValid(slot)
	{
		if (this.getValidSlots().indexOf(slot) >= 0)
		{
			//check if weapon combination is valid

			if(slot == "offhand" && this.actor)
			{
				const main = this.actor.getItem("mainhand");
				if(main)
				{
					return main.getValidSlots().indexOf("offhand") >= 0; //if both items support the offhand slot then the offhand slot can be used
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * 
	 * @returns a object with {"close": false, "ranged": false, "magic": false}. Depending type of attack can be blocked
	 */
	canBlock()
	{
		if (this.type == "weapon")
		{
			const weaponAttr = this.system.attribute;

			
			if(booleanFix(this.system.is_shield))
			{
				return {"close": (weaponAttr=="RES"), "ranged": (weaponAttr=="RES"), "magic": (weaponAttr=="WIL")};
			}

			const isGun = booleanFix(this.system.is_gun);
			const weaponConfigList = isGun ? CONFIG.ORBITAL.fernkampf_attribute : CONFIG.ORBITAL.nahkampf_attribute; 
			

			let canWeaponBlock = weaponConfigList[weaponAttr].can_block;

			if(!isGun)
			{
				if(weaponAttr == "PEZ")
				{
					canWeaponBlock = (getSkillLevel(this.actor, "Nahkämpfer", "Schwertmeister") > 0)
				}
				else if(weaponAttr == "AUR")
				{
					if(this.system.two_handed)
					{
						canWeaponBlock = (getSkillLevel(this.actor, "Nahkämpfer", "Paladin") > 0);
					}
				}
			}
			if(canWeaponBlock)
			{
				return {"close": true, "ranged": false, "magic": false};
			}
		}
		return {"close":false, "ranged": false, "magic": false};
	}
}
