/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
import { Wurfel2D20 } from './../helpers/wurfel.mjs';

export class OrbitalActor extends Actor 
{
	/** @override */
	prepareData() 
	{
		// Prepare data for the actor. Calling the super version of this executes
		// the following, in order: data reset (to clear active effects),
		// prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
		// prepareDerivedData().
		try {
			super.prepareData();
		} catch(err) {
			console.log(err);
		}
	}

	/** @override */
	prepareBaseData()
	{
		// Data modifications in this step occur before processing embedded
		// documents or derived data.
		const actorData = this;
		const systemData = actorData.system;
		
	}

	/**
	 * @override
	 * Augment the actor source data with additional dynamic data. Typically,
	 * you'll want to handle most of your calculated/derived data in this step.
	 * Data calculated in this step should generally not exist in template.json
	 * (such as ability modifiers rather than ability scores) and should be
	 * available both inside and outside of character sheets (such as if an actor
	 * is queried and has a roll executed directly from it).
	 */

	prepareDerivedData() 
	{
		const actorData = this;
		const systemData = actorData.system;
		const flags = actorData.flags.orbital || {};

		// Make separate methods for each Actor type (character, npc, etc.) to keep
		// things organized.
		this._prepareCharacterData(actorData);
		this._prepareNpcData(actorData);

		this._calcAttributeModifier(actorData);

		var totalDNA = 0;
		if(systemData.attributes)
		{
			for (let [key, attribute] of Object.entries(systemData.attributes)) 
			{
				// Calculate the modifier using d20 rules.
				const dice = this._getAttributeDiceFromActor(key);
				dice.addModifierFromActor(this);
				attribute.wurfel = dice.asJson();
				totalDNA += this._calcAttributeCost(attribute.base);
			}
		}
		systemData.total_dna = totalDNA;
		if(systemData.resources && systemData.attributes)
		{
			const oldHP = systemData.resources.health.max;
			const oldIni = systemData.resources.initiative.max;
			const relHealth = systemData.resources.health.value / oldHP;
			const relIni = systemData.resources.initiative.value / oldIni;

			systemData.resources.health.max = systemData.attributes.STR.value + systemData.attributes.RES.value + systemData.attributes.REF.value + systemData.attributes.GES.value + systemData.attributes.AUD.value;
			systemData.resources.initiative.max = systemData.attributes.REF.value + systemData.attributes.INT.value + systemData.attributes.PEZ.value + systemData.attributes.AUD.value;
			systemData.resources.luck.max = systemData.attributes.INT.value + systemData.attributes.WIL.value + systemData.attributes.PEZ.value + systemData.attributes.AUR.value + systemData.attributes.CHA.value;
		
			//cant do this as old healöth Data si always 4, maybe becuase no proper sincing
			/*if(oldHP != systemData.resources.health.max)
			{
				console.log(oldHP , systemData.resources.health.max)
				systemData.resources.health.value = Math.floor(relHealth * systemData.resources.health.max)
				if( relHealth > 0 && systemData.resources.health.value <= 0)
				{
					systemData.resources.health.value = 1
				}
			}
			if(oldIni != systemData.resources.initiative.max)
			{
				console.log(oldIni , systemData.resources.initiative.max)
				systemData.resources.initiative.value = Math.floor(relIni * systemData.resources.initiative.max)
				if( relIni > 0 && systemData.resources.initiative.value <= 0)
				{
					systemData.resources.initiative.value = 1
				}
			}*/
			
		}

		const maxSkillCount = systemData.maxSkillCount || 1;
		//console.log(maxSkillCount);
		var skillCosts = 0;

		if(!systemData.skills)
		{
			systemData.skills = {};
		}
		for(var i=1;i<=maxSkillCount;i++)
		{
			if(!systemData.skills["s"+i])
			{
				systemData.skills["s"+i] = {"value": "-", "subskill":"-", "attribute": "", "wurfel":{"min":21, "max":21}, "level":0};
			}

			if(systemData.skills["s"+i].value)
			{
				var skillEntry = CONFIG.ORBITAL.skills[systemData.skills["s"+i].value];
				if(skillEntry)
				{
					var subskillEntry = skillEntry[systemData.skills["s"+i].subskill];
					if(subskillEntry)
					{
						systemData.skills["s"+i].attribute = subskillEntry;
						systemData.skills["s"+i].level = systemData.skills["s"+i].level || 0;
						skillCosts += 5+ this._calcAttributeCost(systemData.skills["s"+i].level);

						const dice = this._getAttributeDiceFromActor(subskillEntry);
						dice.addModifier(game.i18n.localize("orbital.skill."+systemData.skills["s"+i].value), systemData.skills["s"+i].level);
						dice.addModifierFromActor(this);
						systemData.skills["s"+i].wurfel = dice.asJson();

						if(systemData.skills["s"+i].subskill != "Keine")
						{
							skillCosts += 5;
						}
					}
					else
					{
						systemData.skills["s"+i].attribute = "";
						systemData.skills["s"+i].wurfel = {};
						systemData.skills["s"+i].wurfel.min = 21;
						systemData.skills["s"+i].wurfel.max = 21;
						systemData.skills["s"+i].level = 0;
					}
				}
				else
				{
					systemData.skills["s"+i].subskill = "-";
					systemData.skills["s"+i].attribute = "";
					systemData.skills["s"+i].wurfel = {};
					systemData.skills["s"+i].wurfel.min = 21;
					systemData.skills["s"+i].wurfel.max = 21;
					systemData.skills["s"+i].level = 0;
				}
			}
		}
		systemData.total_sp = skillCosts;


		if(systemData.race)
		{
			if(CONFIG.ORBITAL.races[systemData.race])
			{
				const raceData = CONFIG.ORBITAL.races[systemData.race];
				systemData.age.max = raceData.maxAge;

				let hmin = parseFloat(raceData.height.min.replace(" m", "").replace(",", ".")) * 100
				let hmax = parseFloat(raceData.height.max.replace(" m", "").replace(",", ".")) * 100
				systemData.height_cm.min = hmin
				systemData.height_cm.max = hmax

				systemData.race_info = new Handlebars.SafeString(
					"<div class='grid grid-2col'>"
					+`<span class='grid-span-1'>${game.i18n.localize("orbital.actor.genom")}</span><span class='grid-span-1'>${raceData.genom}</span>`
					+`<span class='grid-span-1'>${game.i18n.localize("orbital.actor.raceskill")}</span><span class='grid-span-1'>${raceData.raceskill || "-"}</span>`
					+`<span class='grid-span-1'>${game.i18n.localize("orbital.actor.character")}</span><span class='grid-span-1'>${raceData.character.join(", ")}</span>`
					+"</div><hr>"
					+`${raceData.desc}`
				);
			}
		}
		let total_weight = 0;
		for (let i of this.items) 
		{
			if(i.system.weight && i.system.quantity)
			{
				if(!i.isEquipped())
				{
					total_weight += i.system.weight * i.system.quantity;
				}
			}
		}

		if(!systemData.total_weight)
		{
			systemData.total_weight = {value: total_weight, max: 4};
		}
		systemData.total_weight.max = 4 + systemData.attributes["AUD"].value * 4 + systemData.attributes["STR"].value * 2;
		systemData.total_weight.value = total_weight;
	}
	
	_onUpdate(data, options, userId) 
	{
		super._onUpdate(data, options, userId);
		const actorData = this;
		const systemData = actorData.system;


		//Apply/Remove effect DEFEATED based on health
		if(systemData.resources.health.value <= 0)
		{
			if(!actorData.statuses.has(CONFIG.specialStatusEffects.DEFEATED))
			{
				actorData.toggleStatusEffect(CONFIG.specialStatusEffects.DEFEATED, {active: true, overlay: false});
			}
		}
		else
		{
			if(actorData.statuses.has(CONFIG.specialStatusEffects.DEFEATED))
			{
				actorData.toggleStatusEffect(CONFIG.specialStatusEffects.DEFEATED, {active: false, overlay: false});
			}
		}
		
		
		//Apply/Remove effect UNCONSCIOUS based on initiative
		if(systemData.resources.initiative.value <= 0)
		{
			if(!actorData.statuses.has(CONFIG.specialStatusEffects.UNCONSCIOUS))
			{
				actorData.toggleStatusEffect(CONFIG.specialStatusEffects.UNCONSCIOUS, {active: true, overlay: false});
			}
		}
		else
		{
			if(actorData.statuses.has(CONFIG.specialStatusEffects.UNCONSCIOUS))
			{
				actorData.toggleStatusEffect(CONFIG.specialStatusEffects.UNCONSCIOUS, {active: false, overlay: false});
			}
		}

	}
	

	//override
	async toggleStatusEffect(statusId, {active, overlay=false}={})
	{

		//If DEFEATED is added or removed adjust health accordingly
		if(statusId == CONFIG.specialStatusEffects.DEFEATED)
		{
			const wasActive = this.effects.filter(e => e.statuses.has(CONFIG.specialStatusEffects.DEFEATED)).length > 0;

			//do the real change to status now and wait for completion befor atemting to update- Otherwise effect will be added twice.
			const ret = super.toggleStatusEffect(statusId, {active, overlay});

			if(!wasActive && (active === undefined || active))
			{
				await ret
				this.update({"system.resources.health.value": 0 });
			}
			else if(wasActive && (active === undefined || !active))
			{
				await ret
				this.update({"system.resources.health.value": 1 });
			}

			return ret
		}
		//same for UNCONSCIOUS
		else if(statusId == CONFIG.specialStatusEffects.UNCONSCIOUS)
		{
			const wasActive = this.effects.filter(e => e.statuses.has(CONFIG.specialStatusEffects.UNCONSCIOUS)).length > 0;

			//do the real change to status now and wait for completion befor atemting to update- Otherwise effect will be added twice.
			const ret = super.toggleStatusEffect(statusId, {active, overlay});

			if(!wasActive && (active === undefined || active))
			{
				await ret
				this.update({"system.resources.initiative.value": 0 });
			}
			else if(wasActive && (active === undefined || !active))
			{
				await ret
				this.update({"system.resources.initiative.value": this.system.attributes["WIL"].value });
			}

			return ret	
		}		
		else
		{
			return super.toggleStatusEffect(statusId, {active, overlay});
		}
			
	}
	
	

	/**
	 * Prepare Character type specific data
	 */
	_prepareCharacterData(actorData) {
		if (actorData.type !== 'character') return;

		// Make modifications to data here. For example:
		const systemData = actorData.system;

		// Loop through ability scores, and add their modifiers to our sheet output.
		for (let [key, ability] of Object.entries(systemData.abilities)) {
			// Calculate the modifier using d20 rules.
			ability.mod = Math.floor((ability.value - 10) / 2);
		}
	}

	/**
	 * Prepare NPC type specific data.
	 */
	_prepareNpcData(actorData) {
		if (actorData.type !== 'npc') return;

		// Make modifications to data here. For example:
		const systemData = actorData.system;
		systemData.xp = systemData.cr * systemData.cr * 100;
	}

	/**
	 * Override getRollData() that's supplied to rolls.
	 */
	getRollData() {
		// Starts off by populating the roll data with `this.system`
		const data = { ...super.getRollData() };

		// Prepare character roll data.
		this._getCharacterRollData(data);
		this._getNpcRollData(data);

		return data;
	}

	/**
	 * Prepare character roll data.
	 */
	_getCharacterRollData(data) {
		if (this.type !== 'character') return;

		// Copy the ability scores to the top level, so that rolls can use
		// formulas like `@str.mod + 4`.
		if (data.abilities) {
			for (let [k, v] of Object.entries(data.abilities)) {
				data[k] = foundry.utils.deepClone(v);
			}
		}

		// Add level for easier access, or fall back to 0.
		if (data.attributes.level) {
			data.lvl = data.attributes.level.value ?? 0;
		}
	}

	/**
	 * Prepare NPC roll data.
	 */
	_getNpcRollData(data) {
		if (this.type !== 'npc') return;

		// Process additional NPC data here.
	}

	_getAttributeFromActor(attribute)
	{
		const systemData = this.system;
		if(systemData.attributes)
		{
			var entry = systemData.attributes[attribute]
			if(entry)
			{
				return entry.value
			}
		}
		return 0
	}

	_getAttributeDiceFromActor(attribute)
	{
		const systemData = this.system;
		if(systemData.attributes)
		{
			var entry = systemData.attributes[attribute]
			if(entry)
			{
				const wurfel = new Wurfel2D20();
				wurfel.addModifier(attribute, entry.base);
				if(entry.bonus > 0)
				{
					wurfel.addModifier(game.i18n.localize("game.actor.attribute.bonus"), entry.bonus);
				}
				wurfel.addModifierFromActor(this);
				return wurfel
			}
		}
		return false
	}

	_calcAttributeCost(skillLevel)
	{
		skillLevel -= 1;
		
		return skillLevel * (1 + skillLevel) / 2 //1 +2 +3 +4 +5
	}

	isEquipped(item)
	{
		//console.log("isEquipped", item, this);
		//console.log(this.system.inventory);
		if(this.system.inventory)
		{
			//console.log("this.system.inventory");
			const slots = item.getValidSlots();
			for(const slot of slots)
			{

				const items = this.getItem(slot);
				//console.log(slot, "->", items);
				if(Array.isArray(items))
				{
					for(const i of items)
					{
						if(i == item)
							return true;
					}
				}
				else
				{
					if(items == item)
						return true;
				}
			}
		}
		return false;
	}

	getItem(slot)
	{
		//console.log("getItem", this.system.inventory);
		if(this.system.inventory)
		{
			//console.log("inventory", slot, this.system.inventory[slot]);
			if(this.system.inventory[slot])
			{
				const it = this.system.inventory[slot];
				//console.log("slot", it);
				if(Array.isArray(it))
				{
					const layered = [];
					const items = this.items;
					it.forEach((x, i) => layered.push(items.get(x)));
					
					return layered;
				}
				else
				{
					return this.items.get(it);
				}
			}
		}
		return null
	}

	async equipItem(item, slot)
	{
		console.log("equipItem", this, item, slot);

		const slotConfig = CONFIG.ORBITAL.equipment_slots[slot];
		let max = 0;
		if(slotConfig)
		{
			max = slotConfig.max || 0;  
		}

		if(max <= 0)
		{
			console.error("Slot max is 0", max);
			return false;
		}

		if(item.isSlotValid(slot))
		{
			if(!this.system.inventory)
			{
				this.system.inventory = {};
			}

			if(max == 1)
			{
				if(item.type == "weapon")
				{
					const twohanded = (item.system.two_handed == "true") || (item.system.two_handed === true);
					if(twohanded)
					{
						if(this.system.inventory["offhand"])
						{
							const offhandItem = this.items.get(this.system.inventory["offhand"]);
							await this.unequipItem(offhandItem, "offhand")
						}
						
					}
				}

				this.system.inventory[slot] = item.id;
				await this.update({'system.inventory' : this.system.inventory});
				return true;
			}
			else
			{
				if(!this.system.inventory[slot])
				{
					this.system.inventory[slot] = [];
				}
				else
				{
					//console.log(this.system.inventory[slot]);
					const r = this.system.inventory[slot].filter((x) => (x !== null && x !== undefined));
					//console.log(r);
					this.system.inventory[slot] = r;
				}

				if(this.system.inventory[slot].indexOf(item.id) >= 0)
				{
					return true;
				}
				else if(this.system.inventory[slot].length +1 <= max)
				{
					this.system.inventory[slot].push(item.id);
					await this.update({'system.inventory' : this.system.inventory});
					return true;
				}
				else
				{
					console.error("already full", this.system.inventory[slot].length +1 , max);
					return false
				}
			}
			
		}
		else
		{
			console.error("Slot is  not valid", slot);
			return false;
		}
	}

	async unequipItem(item, slot)
	{
		console.log("unequipItem", this, item, slot);

		if(this.system.inventory)
		{
			if (this.system.inventory[slot] === item.id)
			{
				this.system.inventory[slot] = null;

				await this.update({'system.inventory' : this.system.inventory});
			}
			else if(Array.isArray(this.system.inventory[slot] ))
			{
				const index = this.system.inventory[slot].indexOf(item.id);
				if(index >= 0)
				{
					delete this.system.inventory[slot][index];
					await this.update({'system.inventory' : this.system.inventory});
				}
			}
		}
		
	}

	_calcAttributeModifier()
	{
		const bonus = {};
		for(let key in CONFIG.ORBITAL.attribute_list)
		{
			bonus[key] = 0;
		}
		this.items.filter((i) => i.isEquipped() && i.system.bonus_attribute).forEach((i) => bonus[i.system.bonus_attribute] += i.system.bonus.value );


		for (let [k, v] of Object.entries(this.system.attributes)) 
		{
			let final_bonus = bonus[k];
			let base_bonus = 0;
			let base_multiplicator = 1;

			for( e in v.modifier)
			{
				let type = e.type || "base_bonus"

				if(type == "base_bonus")
				{
					base_bonus += (e.value || 0)
				}
				else if(type == "final_bonus")
				{
					final_bonus += (e.value || 0)
				}
				else if(type == "base_multiplicator")
				{
					base_multiplicator += (e.value || 0)
				}

			}

			v.value = ((v.base || 1) + base_bonus) * base_multiplicator + final_bonus;
			v.bonus = v.value - v.base;
		}
	}
}
