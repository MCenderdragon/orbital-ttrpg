import {
	onManageActiveEffect,
	prepareActiveEffectCategories,
} from '../helpers/effects.mjs';

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class OrbitalItemSheet extends ItemSheet {
	/** @override */
	static get defaultOptions() 
	{
		return foundry.utils.mergeObject(super.defaultOptions, {
			classes: ['orbital', 'sheet', 'item'],
			width: 520,
			height: 480,
			tabs: [
				{
					navSelector: '.sheet-tabs',
					contentSelector: '.sheet-body',
					initial: 'description',
				},
			],
		});
	}

	/** @override */
	get template() 
	{
		const path = 'systems/orbital/templates/item';
		// Return a single sheet for all item types.
		// return `${path}/item-sheet.hbs`;

		// Alternatively, you could use the following return statement to do a
		// unique item sheet by type, like `weapon-sheet.hbs`.
		return `${path}/item-${this.item.type}-sheet.hbs`;
	}

	/* -------------------------------------------- */

	/** @override */
	getData() 
	{
		// Retrieve base data structure.
		const context = super.getData();

		// Use a safe clone of the item data for further operations.
		const itemData = context.data;

		// Retrieve the roll data for TinyMCE editors.
		context.rollData = this.item.getRollData();

		// Add the item's data to context.data for easier access, as well as flags.
		context.system = itemData.system;
		context.flags = itemData.flags;

		// Prepare active effects for easier access
		context.effects = prepareActiveEffectCategories(this.item.effects);

		const systemData = this.item.system;
		if(!systemData.effects2)
		{
			const old = [];
			for (let e of this.item.effects) 
			{
				old.push(structuredClone(e));
			}
			systemData.effects2 = old;
			console.log("effects2", systemData.effects2);
		}

		context.effects2 = prepareActiveEffectCategories(systemData.effects2);
		console.log("conetxt.effects2", context.effects2);


		context.spell_types = {"AUR": "orbital.spell.types.AUR", "INT": "orbital.spell.types.INT"};

		context.attribute_list = CONFIG.ORBITAL.attribute_list;

		if(context.item.type == "weapon" || context.item.type == "supportweapon")
		{
			context.two_handed_list = {"true": "orbital.weapon.two_handed.true", "false": "orbital.weapon.two_handed.false"};
			context.is_gun_list = {"true": "orbital.weapon.is_gun.true", "false": "orbital.weapon.is_gun.false"};
			context.is_shield_list = {"true": "orbital.weapon.is_shield.true", "false": "orbital.weapon.is_shield.false"};
			context.is_deadly_list = {"true": "orbital.weapon.is_deadly.true", "false": "orbital.weapon.is_deadly.false"};
			
		}
		else if(context.item.type == "spell")
		{
			context.has_effect_list = {"true": "orbital.spell.has_effect.true", "false": "orbital.spell.has_effect.false"};
		}
		
		context.slot_list = {};
		for (let [key, ability] of Object.entries(CONFIG.ORBITAL.equipment_slots)) 
		{
			if(ability.isArmor)
				context.slot_list[key] = "orbital.equipment.names."+key;
		}	
		


		console.log("item#getData", context, this);

		return context;
	}

	/* -------------------------------------------- */

	/** @override */
	activateListeners(html) 
	{
		super.activateListeners(html);

		// Everything below here is only needed if the sheet is editable
		if (!this.isEditable) return;

		// Roll handlers, click handlers, etc. would go here.

		// Active Effect management
		html.on('click', '.effect-control', (ev) =>
			onManageActiveEffect(ev, this.item)
		);

		html.on('click', '.spell-roll', this._onSpellRoll.bind(this));
	}

	getAttributeFrom(actor, attribute)
	{
		const attr = actor.system.attributes[attribute];
		if(attr)
		{
			return attr
		}
		else
			return null
	}

	async _onSpellRoll(event)
	{
		event.preventDefault();
		const header = event.currentTarget;
		// Get the type of item to create.
		const type = header.dataset.type;

		if(type == "spell")
		{
			console.log(this);

			const attr = this.object.system.attribute;
			if(attr)
			{
				let attrRange = this.getAttributeFrom(this.object.actor, attr)
				const min = attrRange.wurfel.min - this.object.system.level.value;
				const max = attrRange.wurfel.max + this.object.system.level.value;

				let label = `[Aura] ${this.object.name} ${attr} (${min} - ${max})`;
				let roll = new Roll("2d20", this.actor.getRollData());

				await roll.roll();
				console.log(roll);

				roll.toMessage({
					user: game.user.id,
					speaker: ChatMessage.getSpeaker({ actor: this.actor }),
					flavor: label,
					rollMode: game.settings.get('core', 'rollMode')
				});


				
				const dieA = roll.terms[0].results[0].result;
				const dieB = roll.terms[0].results[1].result;
				let text = "göl";
				if(dieA == dieB)//krit success
				{
					text = "krit. Erfolg";
				}
				else if(dieA == 20 || dieB == 20)
				{
					text = "krit. Fail";
				}
				else if(min <= (dieA+dieB) && (dieA+dieB) <= max)
				{
					text = "Erfolg";
				}
				else
				{
					text = "Fail";
				}

				let content = `<div>${text}</div>`
				ChatMessage.create({content:content, speaker: ChatMessage.getSpeaker({ actor: this.actor })})
				
				
			}
		}
	}
}
