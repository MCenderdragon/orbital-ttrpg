import { ORBITAL } from '../helpers/config.mjs';
import {
	onManageActiveEffect,
	prepareActiveEffectCategories,
} from '../helpers/effects.mjs';

import {calcAttack, calcArmor, booleanFix} from '../helpers/combat.mjs'

import {
	open
} from '../helpers/actor-attack-dialog.mjs';
import { Wurfel2D20 } from '../helpers/wurfel.mjs';

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class OrbitalActorSheet extends ActorSheet 
{
	constructor(options = {}) 
	{
		super(options);
	}
	
	/** @override */
	static get defaultOptions() 
	{
		return foundry.utils.mergeObject(super.defaultOptions, {
			classes: ['orbital', 'sheet', 'actor'],
			width: 925,
			height: 666,
			tabs: [
				{
					navSelector: '.sheet-tabs',
					contentSelector: '.sheet-body',
					initial: 'features',
				},
			],
		});
	}

	/** @override */
	get template() {
		return `systems/orbital/templates/actor/actor-${this.actor.type}-sheet.hbs`;
	}

	/* -------------------------------------------- */

	/** @override */
	getData() 
	{
		console.log("getData!");
		// Retrieve the data structure from the base sheet. You can inspect or log
		// the context variable to see the structure, but some key properties for
		// sheets are the actor object, the data object, whether or not it's
		// editable, the items array, and the effects array.
		const context = super.getData();

		// Use a safe clone of the actor data for further operations.
		const actorData = context.data;

		// Add the actor's data to context.data for easier access, as well as flags.
		context.system = actorData.system;
		context.flags = actorData.flags;

		// Prepare character data and items.
		if (actorData.type == 'player') {
			this._prepareItems(context);
			this._prepareCharacterData(context);
		}

		// Prepare NPC data and items.
		if (actorData.type == 'npc') {
			this._prepareItems(context);
		}

		// Add roll data for TinyMCE editors.
		context.rollData = context.actor.getRollData();

		// Prepare active effects
		context.effects = prepareActiveEffectCategories(
			// A generator that returns all effects stored on the actor
			// as well as any items
			this.actor.allApplicableEffects()
		);

		const maxSkillCount = actorData.system.maxSkillCount || 1;
		const systemData = actorData.system;

		if(!systemData.skills)
		{
			systemData.skills = {}
		}

		for(var i=1;i<=maxSkillCount;i++)
		{
			if(!( ("s"+i) in systemData.skills))
			{
				systemData.skills["s"+i] = {"value": "-", "subskill":"-", "attribute": "", "wurfel":{"min":21, "max":21}};
			}
		}
		
		context.available_skills = {};
		systemData.available_subskills = {};
		for (let [key, attribute] of Object.entries(CONFIG.ORBITAL.skills)) 
		{
			context.available_skills[key] = "orbital.skill." + key
		}

		for (let [pkey, pattribute] of Object.entries(systemData.skills)) 
		{
			pattribute.available_subskills = {}
			if(CONFIG.ORBITAL.skills[pattribute.value])
			{	
				for (let [key, attribute] of Object.entries(CONFIG.ORBITAL.skills[pattribute.value])) 
				{
					pattribute.available_subskills[key] = "orbital.subskill." + key
				}
			}
		}

		context.available_races = {}
		for (let [key, value] of Object.entries(CONFIG.ORBITAL.races)) 
		{
			let available = false
			if (actorData.type == 'player') 
			{
				available = (value.playerRace == 1)
			}
			else if (actorData.type == 'npc') 
			{
				available = (value.companionRace == 1)
			}
			if(available)
				context.available_races[key] = "orbital.race." + key
		}

		//console.log(this.actor.statuses.has('invisible'));
		context.combat = calcAttack(this.actor);

		context.armor = calcArmor(this.actor);


		console.log(context);


		return context;
	}

	/**
	 * Organize and classify Items for Character sheets.
	 *
	 * @param {Object} actorData The actor to prepare.
	 *
	 * @return {undefined}
	 */
	_prepareCharacterData(context) {
		// Handle ability scores.
		
	}

	/**
	 * Organize and classify Items for Character sheets.
	 *
	 * @param {Object} actorData The actor to prepare.
	 *
	 * @return {undefined}
	 */
	_prepareItems(context) 
	{
		console.log("_prepareItems");
		// Initialize containers.
		const gear = [];
		const features = [];
		const spells = {
			0: [],
		};
		const equipment = {
			"mainhand":[],
			"offhand": []
		};

		const equipment_hand = [];

		for (let [slot, entries] of Object.entries(ORBITAL.equipment_slots) )
		{
			equipment[slot] = [];
		}

		const slot_limits = {};
		for (let [slot, entries] of Object.entries(ORBITAL.equipment_slots))
		{
			slot_limits[slot] = {max: entries.max || 0, value: 0};
		}

		// Iterate through items, allocating to containers
		for (let i of context.items) 
		{
			i.img = i.img || Item.DEFAULT_ICON;
			i.actorUUID = this.actor.uuid;
			i.uuid = this.actor.items.get(i._id).uuid;
			// Append to gear.


			if (i.type === 'item') {
				gear.push(i);
			}
			// Append to features.
			else if (i.type === 'feature') {
				features.push(i);
			}
			// Append to spells.
			else if (i.type === 'spell') {
				if (i.system.spellLevel != undefined) {
					spells[i.system.spellLevel].push(i);
				}
				else
				{
					spells[0].push(i);
				}
			}
			else if(i.type == 'weapon')
			{
				console.log(i, this, this.actor.items);
				i.system.isEquipped = this.actor.items.get(i._id).isEquipped();
				i.system.equippedSlot = this.actor.items.get(i._id).getEquippedSlot();
				const validSlots = this.actor.items.get(i._id).getValidSlots();

				i.attackStats = "🩸"+i.system.damage_lethal+ "⚡" + i.system.damage_stun;
				if(booleanFix(i.system.is_shield))
				{
					i.attackStats = "🛡️"+i.system.damage_lethal+ "🧠" + i.system.damage_stun;
				}
				i.attackStats += i.system.isEquipped ? ("(👜" + i.system.weight+")") : ("👜" + i.system.weight) ;

				equipment["mainhand"].push(i);

				const twohanded = i.system.two_handed == "true" || i.system.two_handed === true;
				if(!twohanded)
				{
					
					equipment["offhand"].push(i);
				}
				if(i.system.isEquipped)
					slot_limits[i.system.equippedSlot].value += 1; 
				

				i.system.equipped_at = "inventory";
				if(i.system.isEquipped)
				{
					i.system.equipped_at = i.system.equippedSlot;
				}
				i.validSlots = {"inventory": "orbital.equipment.names.inventory"};
				validSlots.forEach(slot => (i.validSlots[slot] = "orbital.equipment.names." + slot));

				equipment_hand.push(i);

			}
			else if(i.type == 'armor')
			{
				if(!i.system.slot)
				{
					i.system.slot = "NONE";
				}
				i.system.isEquipped = this.actor.items.get(i._id).isEquipped();
				i.system.equippedSlot = this.actor.items.get(i._id).getEquippedSlot();

				const isPhysical = CONFIG.ORBITAL.armor_phys.includes(i.system.attribute);
				const isMind = CONFIG.ORBITAL.armor_mind.includes(i.system.attribute);

				i.attackStats = (isPhysical ? "🛡️" : "")+(isMind ? "🧠" : "")+i.system.armor.value;
				i.attackStats += i.system.isEquipped ? ("(👜" + i.system.weight+")") : ("👜" + i.system.weight) ;

				if(!equipment[i.system.slot])
				{
					equipment[i.system.slot] = []
				}
				
				equipment[i.system.slot].push(i);
				slot_limits[i.system.slot].value += i.system.isEquipped ? 1 : 0; 
			}
			else if(i.type == 'supportweapon')
			{
				i.attackStats = "🩸"+i.system.damage_lethal+ "⚡" + i.system.damage_stun;
				i.attackStats += i.system.isEquipped ? ("(👜" + i.system.weight+")") : ("👜" + i.system.weight) ;
				equipment["support"].push(i);
				slot_limits["support"].value += i.system.isEquipped ? 1 : 0; 
			}

			if(i.system.bonus_attribute && i.system.bonus && i.system.bonus.value > 0)
			{
				i.bonus_text =  i.system.bonus_attribute + " " + (i.system.bonus.value > 0 ? "+" : "") + i.system.bonus.value;
			}
		}

		// Assign and return
		context.gear = gear;
		context.features = features;
		context.spells = spells;
		delete equipment.mainhand;
		delete equipment.offhand
		context.equipment = equipment;
		context.equipment_hand = equipment_hand;
		context.slot_limits = slot_limits;
	}

	/* -------------------------------------------- */

	/** @override */
	activateListeners(html) 
	{
		super.activateListeners(html);

		// Render the item sheet for viewing/editing prior to the editable check.
		html.on('click', '.item-edit', (ev) => {
			const li = $(ev.currentTarget).parents('.item');
			const item = this.actor.items.get(li.data('itemId'));
			item.sheet.render(true);
		});

		// -------------------------------------------------------------
		// Everything below here is only needed if the sheet is editable
		if (!this.isEditable) return;

		// Add Inventory Item
		html.on('click', '.item-create', this._onItemCreate.bind(this));

		// Delete Inventory Item
		html.on('click', '.item-delete', (ev) => {
			const li = $(ev.currentTarget).parents('.item');
			const item = this.actor.items.get(li.data('itemId'));
			item.delete();
			li.slideUp(200, () => this.render(false));
		});

		// Active Effect management
		html.on('click', '.effect-control', (ev) => {
			const row = ev.currentTarget.closest('li');
			const document =
				row.dataset.parentId === this.actor.id
					? this.actor
					: this.actor.items.get(row.dataset.parentId);
			onManageActiveEffect(ev, document);
		});

		// Rollable abilities.
		html.on('click', '.rollable', this._onRoll.bind(this));

		// Drag events for macros.
		if (this.actor.isOwner) {
			let handler = (ev) => this._onDragStart(ev);
			html.find('li.item').each((i, li) => {
				if (li.classList.contains('inventory-header')) return;
				li.setAttribute('draggable', true);
				li.addEventListener('dragstart', handler, false);
			});
		}

		html.on('click', '.skill-roll', this._onSkillRoll.bind(this));

		html.on('click', '.item-equip', this._onItemEquip.bind(this));

		html.on('change', '.sel_equ_hand', this._onSelectHandChange.bind(this));

		html.on('click', '.actor-attack', this._onActorAttack.bind(this));
		
	}

	/**
	 * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
	 * @param {Event} event   The originating click event
	 * @private
	 */
	async _onItemCreate(event) {
		event.preventDefault();
		const header = event.currentTarget;
		// Get the type of item to create.
		let type = header.dataset.type;

		if(type=="equipment")
		{
			if(header.dataset.slot =="mainhand" || header.dataset.slot =="offhand")
			{
				type = "weapon";
			}
			else if(header.dataset.slot =="support")
			{
				type = "supportweapon";
			}
			else
			{
				type = "armor";
			}
		}

		// Grab any data associated with this control.
		const data = foundry.utils.duplicate(header.dataset);
		// Initialize a default name.
		const name = `New ${type.capitalize()}`;
		// Prepare the item object.
		const itemData = {
			name: name,
			type: type,
			system: data,
		};
		if(header.dataset.slot && CONFIG.ORBITAL.equipment_slots[header.dataset.slot])
		{
			const conf = CONFIG.ORBITAL.equipment_slots[header.dataset.slot];
			if(conf.weight)
			{
				itemData.system.weight = conf.weight
			}
		}
		// Remove the type from the dataset since it's in the itemData.type prop.
		delete itemData.system['type'];

		// Finally, create the item!
		return await Item.create(itemData, { parent: this.actor });
	}

	/**
	 * Handle clickable rolls.
	 * @param {Event} event   The originating click event
	 * @private
	 */
	_onRoll(event) {
		event.preventDefault();
		const element = event.currentTarget;
		const dataset = element.dataset;

		// Handle item rolls.
		if (dataset.rollType) {
			if (dataset.rollType == 'item') {
				const itemId = element.closest('.item').dataset.itemId;
				const item = this.actor.items.get(itemId);
				if (item) return item.roll();
			}
		}

		// Handle rolls that supply the formula directly.
		if (dataset.roll) {
			let label = dataset.label ? `[ability] ${dataset.label}` : '';
			let roll = new Roll(dataset.roll, this.actor.getRollData());
			roll.toMessage({
				speaker: ChatMessage.getSpeaker({ actor: this.actor }),
				flavor: label,
				rollMode: game.settings.get('core', 'rollMode'),
			});
			return roll;
		}
	}

	/**
	 * 
	 * @param {Wurfel2D20} wurfel 
	 * @param {String} displayText 
	 * @param {String} type 
	 * @param {Function} extraTextCallback
	 */
	async _do2D20Roll(wurfel, displayText, type, extraTextCallback = null)
	{
		let bonusMalus = 0;
		if(wurfel.dice)
		{
			const dice = wurfel.dice;
			dice.addModifierFromActor(this.actor);
			wurfel = dice.asJson();
			bonusMalus = 0;
		}
		else
		{	
			let values = [1,2,4,6];
			for (const val of values)
			{
				if(this.actor.statuses.has(`orbital.roll.up.${val}`))
					bonusMalus += val;
				if(this.actor.statuses.has(`orbital.roll.down.${val}`))
					bonusMalus -= val;
			}
				}
		const tooltip = "21" + (wurfel.tooltip ? ` &#xB1; ${wurfel.tooltip}`+(bonusMalus!=0 ? ` ${bonusMalus>0?"Bonus: +":"Malus: "}${bonusMalus}` : "") : "")
		let label = `[${game.i18n.localize("orbital.actor."+type)}] ${displayText} (${wurfel.min-bonusMalus} - ${wurfel.max+bonusMalus})`;
		let roll = new Roll("2d20", this.actor.getRollData());
		
		await roll.roll();
		//roll.toMessage({
		//	user: game.user.id,
		//	speaker: ChatMessage.getSpeaker({ actor: this.actor }),
		//	flavor: label,
		//	rollMode: game.settings.get('core', 'rollMode')
		//});
			
		const dieA = roll.terms[0].results[0].result;
		const dieB = roll.terms[0].results[1].result;
		let text = "göl";
		let success = false;
		let krit = false;
		if(dieA == dieB)//krit success
		{
			text = "krit. Erfolg";
			success = true;
			krit = true;
		}
		else if( (dieA == 20) || (dieB == 20))
		{
			text = "krit. Fail";
			success = false;
			krit = true;
		}
		else if((wurfel.min-bonusMalus) <= (dieA+dieB) && (dieA+dieB) <= (wurfel.max+bonusMalus))
		{
			text = "Erfolg";
			success = true;
			krit = false;
		}
		else
		{
			text = "Fail";
			success = false;
			krit = false;
		}

		let content = `<div>${text}</div>`
		//ChatMessage.create({content:content, speaker: ChatMessage.getSpeaker({ actor: this.actor })});
		//foundry.audio.AudioHelper.play({src: "sounds/dice.wav", volume: 1, autoplay: true, loop: false}, true);
		const extraText = extraTextCallback ? extraTextCallback(dieA, dieB, success, krit, wurfel) : ""; 
		ChatMessage.create({
		content: `
		<div class="dice-roll">
			<div class="dice-result">
				<div class="dice-tooltip" style="display: block;">
					<section class="tooltip-part">
						<div class="dice">
							<ol class="dice-rolls">
								<li class="roll die d20">${dieA}</li>
								<li class="roll die d20">${dieB}</li>
							</ol>
						</div>
					</section>
				</div>
				<h4 class="dice-total"> ${text} (${dieA+dieB})</h4>
			</div>
			<div data-tooltip='${tooltip}'>${label}</div>
			${extraText}
		</div>
		`,
		speaker: ChatMessage.getSpeaker({ actor: this.actor }),
		sound: CONFIG.sounds.dice
		});
	}

	async _onSkillRoll(event)
	{
		event.preventDefault();
		const header = event.currentTarget;
		// Get the type of item to create.
		const type = header.dataset.type;

		let skill = false;
		if(type == "skill")
		{
			const skillKey = header.dataset.skill;
			skill = this.actor.system.skills[skillKey]
		}
		if(type == "attribute")
		{
			const attribute = header.dataset.attribute;
			skill = this.actor.system.attributes[attribute];
			skill.attribute = attribute;
		}	

		if(skill)
		{
			this._do2D20Roll(skill.wurfel, `${skill.value} ${skill.attribute}`, type);
		}
	}

	async _onItemEquip(event)
	{
		event.preventDefault();
		const header = event.currentTarget;
		// Get the type of item to create.
		const itemid = header.dataset.itemid;
		const slot = header.dataset.itemslot;
		console.log(this);
		console.log(this.actor)
		const item = this.actor.items.get(itemid);

		if(item)
		{
			if(item.type == "weapon" || item.type == "armor")
			{
				if(!item.isEquipped())
					console.log("result", await this.actor.equipItem(item, slot));
				else
					console.log("result", await this.actor.unequipItem(item, slot));
				//console.log(this.actor);

				//await item.update({'system.isEquipped': item.isEquipped()});
				//console.log(item);

				this.actor.render(true);
			}
		}
	}

	async _onSelectHandChange(event)
	{
		event.preventDefault();

		const header = event.currentTarget;
		// Get the type of item to create.
		const itemid = header.dataset.itemid;
		//console.log(this);
		//console.log(this.actor)
		const item = this.actor.items.get(itemid);
		const oldslot = item.getEquippedSlot();

		if(header.value == "inventory")
		{
			console.log("result", await this.actor.unequipItem(item, oldslot));
		}
		else
		{
			console.log("result", await this.actor.equipItem(item, header.value));
		}

		this.actor.render(true);

	}

	async _onActorAttack(event)
	{
		event.preventDefault();

		open(this);

		
	}
}


