#!/bin/sh

if [ -n "${FVTT_INSTALL_DIR}" ] && [ -d  "${FVTT_INSTALL_DIR}" ]; then
	echo "Found FVTT_INSTALL_DIR '$FVTT_INSTALL_DIR'" 
	fvtt configure set installPath "$FVTT_INSTALL_DIR"
elif [ -d  "C:/Program Files/Foundry Virtual Tabletop" ]; then
	fvtt configure set installPath "C:/Program Files/Foundry Virtual Tabletop"
else
	echo "Did not found 'Foundry Virtual Tabletop' installation dir, searched at 'C:/Program Files/Foundry Virtual Tabletop', please define the path in FVTT_INSTALL_DIR"
	exit  
fi

./cli_pack_all.sh
echo "Starting Foundry VTT"
fvtt launch
./cli_unpack_all.sh
pause