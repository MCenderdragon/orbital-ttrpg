import csv

with open("skills.tsv", 'r') as f:
		text = f.read()
		lines = text.splitlines()
		grid = []
		for line in lines:
			grid.append(line.split("\t"))

		skills = {}
		for line, row in enumerate(grid):
			if line % 2 == 1:
				for pos, column in enumerate(row):
					if pos > 0 and column != "":
						specialName = column

						if( (line+1) < len(grid)):
							attribute = grid[line+1][pos]
							baseSkill = row[0]

							#print(baseSkill, specialName, attribute)

							if not baseSkill in skills:
								skills[baseSkill] = {}

							skills[baseSkill][specialName] = attribute

		print(skills)

print()
					
with open("lebenformen.tsv", 'r') as f:
		text = f.read()
		lines = text.splitlines()
		grid = []
		for line in lines:
			grid.append(line.split("\t"))

		races = {}

		for line, row in enumerate(grid):
			if line > 0:
				entry = {}
				name = row[0]

				if name != "":

					entry["maxAge"] = row[1]
					entry["adultAge"] = row[2]
					entry["sex"] = [row[3], row[4]]
					entry["genom"] = row[5]
					entry["height"] = {"min" : row[6], "max": row[7]}
					entry["character"] = [row[8], row[9]]
					entry["raceskill"] = row[10]
					entry["canFly"] = 1 if (row[11] == "ja") else 0
					entry["playerRace"] = 1 if row[12] == "Spieler" else 0
					entry["companionRace"] = 1 if row[13] == "Gefärte" else 0

					entry["desc"] = row[14]


					races[name] = entry

		print(races)

