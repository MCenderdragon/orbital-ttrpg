#!/bin/sh

#set the Data Path aka install dir of Foundry
# usually its ...\AppData\Local\FoundryVTT
# this file is in ...\AppData\Local\FoundryVTT\Data\systems\orbital\
# $0 is the path of this file, %c-li_config remvoes the file nmae
echo $(realpath ${0%cli_config.sh}/../../../)
fvtt configure set dataPath "$(realpath ${0%cli_config.sh}/../../../)"
# we want to wokr on the orbital System
fvtt package workon "orbital" --type "System"
